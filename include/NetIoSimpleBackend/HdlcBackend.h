#ifndef NETIOSIMPLEBACKEND_H
#define NETIOSIMPLEBACKEND_H

/** @author Piotr Nikiel
    @date   18-Sep-2016
 */

#include <memory> // for shared_ptr
#include <boost/thread.hpp>

#include <Hdlc/Backend.h>
#include <Hdlc/Payload.h>
#include <Hdlc/LocalStatistician.h>
#include <NetIoSimpleBackend/SimpleNetioSpecificAddress.h>

#include <netio.hpp>
#include <hdlc_coder/hdlc.hpp>

namespace NetIoSimpleBackend
{

class HdlcBackend : public Hdlc::Backend
{
public:
	HdlcBackend (const std::string& specificAddress);
	virtual ~HdlcBackend ();

	virtual void send (const Hdlc::Payload &request );
    virtual void send (
              const std::vector<Hdlc::Payload> &requests,
              const std::vector<unsigned int> times
          );
	virtual void subscribeReceiver ( ReceiveCallBack f ) { m_receivers.push_back(f); }
	virtual const std::string& getAddress ( ) const { return m_originalSpecificAddress; }

	virtual Hdlc::BackendStatistics getStatistics () const { return m_statistician.toBackendStatistics(); }

	virtual void sendHdlcControl (uint8_t hdlcControl);

	void cleanUp ();

private:
	std::string m_originalSpecificAddress;
	std::shared_ptr<const SpecificAddress> m_specificAddress;
	netio::context m_netioContext;
	boost::thread m_netioThread;
	netio::low_latency_send_socket& m_netioRequestSocket;
	netio::low_latency_subscribe_socket m_netioReplySocket;
	std::list<ReceiveCallBack> m_receivers;

	Hdlc::LocalStatistician m_statistician;

	unsigned int m_seqnr; // HDLC sequence number

    bool m_replySocketSubscribed;

	void netioMainThread(void);	
	void replyHandler( netio::endpoint&, netio::message& );

	std::mutex m_transmitSynchronizationMutex;
};


}

#endif // NETIOSIMPLEBACKEND
