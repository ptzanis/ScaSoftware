/*
 * HdlcBackendCommon.h
 *
 *  Created on: Oct 25, 2019
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 *      		Piotr Nikiel (pnikiel@cern.ch)
 *
 *      Description : Common utility functions used by netIO HDLC backend
 */

#pragma once

#include <vector>
#include <iterator>

namespace NetIoSimpleBackend
{

typedef std::vector<uint8_t>::iterator FrameIterator;

const unsigned int NETIO_FRAME_HDLC_START_OFFSET = 8;  // HDLC frame shall be placed starting from this index
const unsigned int HDLC_FRAME_PAYLOAD_OFFSET = 2;   // HDLC payload offset in HDLC frame
const unsigned int HDLC_FRAME_TRAILER_SIZE = 2;   // HDLC trailer size in HDLC frame

/* The following constant relates to Henk B. method of achieving (relatively small - up to say 50 us) delays on the HDLC-encoded elink.
 * A good reading on Henk's method is in: https://its.cern.ch/jira/browse/FLX-581
 * The constant below defines how many zeros to put per microsecond of delay.
 * HDLC Elink throughput is 80Mbps, so 80 bits goes through each 1us.  80bits is 10 bytes that's why we insert 10 bytes to delay by 1 us.
 */
const unsigned int FLX_ZEROS_PER_MICROSECOND = 10;

bool validateFcs( const FrameIterator, const FrameIterator);
std::string vectorAsHex ( const std::vector<uint8_t> & v );
bool validateFcs( const std::vector<uint8_t>& hdlcFrame );

}
