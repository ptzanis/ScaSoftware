/*
 * XilinxJtagProgrammer.h
 *
 *  Created : 18 Jun 2018
 *  Author  : pnikiel
 *  Extended: Dec 2019, henkb
 */

#ifndef INCLUDE_HENKSJTAG_XILINXJTAGPROGRAMMER_H_
#define INCLUDE_HENKSJTAG_XILINXJTAGPROGRAMMER_H_

#include <Sca/Sca.h>
#include <HenksJtag/JtagPort.h>

namespace HenksJtag
{

class BitFile;

class XilinxJtagProgrammer
{
public:
    XilinxJtagProgrammer( Sca::Sca& sca,
                          unsigned int freqMhz = 20.0,
                          unsigned int maxGroupSize = 240 );
    virtual ~XilinxJtagProgrammer( );

    bool programFromBitFile( const std::string& bitfile,
                             int devs_before = 0, int ibits_before = 0,
                             int devs_after = 0, int ibits_after = 0,
                             int dev_instr = -1 );

    bool programFromBlob( const std::string& blob,
                          int devs_before = 0, int ibits_before = 0,
                          int devs_after = 0, int ibits_after = 0,
                          int dev_instr = -1 );

    bool program( BitFile& bitfile );

    bool program( BitFile& bitfile,
                  int devs_before, int ibits_before,
                  int devs_after,  int ibits_after,
                  int dev_instr = -1 );

    uint32_t readId( );

    uint32_t readId( int devs_before, int ibits_before,
                     int devs_after,  int ibits_after,
                     int dev_instr );

    static std::string idCodeToString( uint32_t id );

private:
    HenksJtag::JtagPort m_jtagPort;
    unsigned int        m_freqMhz;
};

} /* namespace HenksJtag */

#endif /* INCLUDE_HENKSJTAG_XILINXJTAGPROGRAMMER_H_ */
