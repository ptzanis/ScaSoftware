/*
 * GPIO.h
 *
 *  Created on: September 7, 2016
 *      Author: aikoulou, based on pnikiel
 */

#ifndef SCASIMULATOR_GPIO_H_
#define SCASIMULATOR_GPIO_H_

#include <ScaSimulator/ScaChannel.h>



namespace ScaSimulator
{

class GPIO: public ScaChannel
{
public:

	GPIO( unsigned char channelId, HdlcBackend *myBackend );
	virtual ~GPIO ();

	virtual void onReceive( const Sca::Request &request );

private:
	uint32_t m_DATAOUT   ;// read/write: this is the data out register
	uint32_t m_DATAIN    ;// read      : this is the data in register
	uint32_t m_DIRECTION ;// read/write: direction of pad. 1=output, 0=input
	uint32_t m_INTENABLE ;// read/write: this BIT is to enable/disable interrupts
	uint32_t m_INTSEL    ;// read/write: select which pads are interrupts (these should be defined as inputs)
	uint32_t m_INTTRIG   ;// read/write: select if interrupts are on rising(1) or falling(0) edge
	uint32_t m_CLKSEL    ;// read/write: BIT: 0=internal 40MHz, 1=external strobe signal from GPIO_CLK pad
	uint32_t m_EDGESEL   ;// read/write: latch to rising(1) or falling(0) edge of ext. clock

	void sendReadReply(const Sca::Request& request,uint32_t);
	void sendWriteReply(const Sca::Request& request,uint32_t,uint32_t);
	uint32_t getRegisterFromCommand(uint32_t);
	void setRegisterFromCommand(uint32_t,uint32_t);
	bool isGpioReadCommand(uint32_t);
	bool isGpioWriteCommand(uint32_t);
};

}

#endif /* SCASIMULATOR_GPIO_H_ */
