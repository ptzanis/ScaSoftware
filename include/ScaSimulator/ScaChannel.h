/*
 * ScaChannel.h
 *
 *  Created on: May 4, 2016
 *      Author: pnikiel
 */

#ifndef SCASIMULATOR_SCACHANNEL_H_
#define SCASIMULATOR_SCACHANNEL_H_

#include <boost/chrono.hpp>

#include <ScaCommon/except.h>
#include <Sca/Request.h>
#include <Sca/Reply.h>

namespace Sca
{
	// forward-defs
	class Frame;
}

namespace ScaSimulator
{

// forward-defs
class HdlcBackend;

class ScaChannel
{
public:
	ScaChannel( unsigned char channelId, HdlcBackend *myBackend ):
		m_channelId(channelId),
		m_busyUntil(boost::chrono::steady_clock::now()),
		m_myBackend(myBackend) {}
	virtual ~ScaChannel () {}

	virtual void onReceive( const Sca::Request &request ) = 0;
	unsigned char channelId () { return m_channelId; }
	bool isBusy () { return boost::chrono::steady_clock::now() < m_busyUntil; }
protected:
	void keepBusyFor ( boost::chrono::microseconds t ) { m_busyUntil = boost::chrono::steady_clock::now() + t; }
	void sendReply ( const Sca::Request & request, Sca::Reply & reply, boost::chrono::microseconds t );
	void sendReplyLaterKeepBusyFor ( const Sca::Request & request, Sca::Reply & reply, double delaySeconds );
	HdlcBackend * backend() const { return m_myBackend; }
private:
	unsigned char m_channelId;
	boost::chrono::steady_clock::time_point m_busyUntil; // until when this channel is busy
	HdlcBackend * m_myBackend;


};



}

#endif /* SCASIMULATOR_SCACHANNEL_H_ */
