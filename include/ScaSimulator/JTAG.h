/*
 * JTAG.h
 *
 *  Created on: February 2, 2017
 *      Author: aikoulou, based on pnikiel
 */

#ifndef SCASIMULATOR_JTAG_H_
#define SCASIMULATOR_JTAG_H_

#include <ScaSimulator/ScaChannel.h>



namespace ScaSimulator
{

class JTAG: public ScaChannel
{
public:

	JTAG( unsigned char channelId, HdlcBackend *myBackend );
	virtual ~JTAG ();

	virtual void onReceive( const Sca::Request &request );

private:
	uint16_t m_CONTROL   ;// read/write: control register
	uint16_t m_FREQUENCY ;// read/write: frequency divider register
	uint32_t m_TDO0      ;// read/write: output buffer register
	uint32_t m_TDO1      ;
	uint32_t m_TDO2      ;
	uint32_t m_TDO3      ;
	uint32_t m_TDI0      ;// read      : input buffer register
	uint32_t m_TDI1      ;
	uint32_t m_TDI2      ;
	uint32_t m_TDI3      ;
	uint32_t m_TMS0      ;// read/write: select which pads are interrupts (these should be defined as inputs)
	uint32_t m_TMS1      ;
	uint32_t m_TMS2      ;
	uint32_t m_TMS3      ;
	
	void sendReadReply(const Sca::Request& request,uint32_t);
	void sendWriteReply(const Sca::Request& request,uint32_t,uint32_t);
	uint32_t getRegisterFromCommand(uint32_t);
	void setRegisterFromCommand(uint32_t,uint32_t);
	bool isJtagReadCommand(uint32_t);
	bool isJtagWriteCommand(uint32_t);

	void jtag_Go();//start transmission (flush/shift TDI/TDO)
	void jtag_Go_m();//jtag_go but w/o notification at end of communication
};

}

#endif /* SCASIMULATOR_JTAG_H_ */
