#ifndef INCLUDE_SCA_SCADAC_H_
#define INCLUDE_SCA_SCADAC_H_

namespace Sca
{

	class Dac
	{
	public:
		Dac (SynchronousService& ss): m_synchronousService(ss) {}

		void writeRegister(::Sca::Constants::Dac dacChannel,uint8_t writeValue);
		uint8_t readRegister(::Sca::Constants::Dac dacChannel);
		float getVoltageEstimateAtRegister(::Sca::Constants::Dac dacChannel);
		float getVoltageStepEstimate();
		void incrementChannelByStep(::Sca::Constants::Dac dacChannel);
		void decrementChannelByStep(::Sca::Constants::Dac dacChannel);
		void setVoltageOnChannel(::Sca::Constants::Dac dacChannel,float voltageSetValue);

	private:
		SynchronousService& m_synchronousService;
		::Sca::Constants::Commands getWriteCommandFromRegister(::Sca::Constants::Dac dacChannel);
		::Sca::Constants::Commands getReadCommandFromRegister(::Sca::Constants::Dac dacChannel);
	};

}

#endif /* INCLUDE_SCA_SCADAC_H_ */
