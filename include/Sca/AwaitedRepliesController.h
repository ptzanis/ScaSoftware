/*
 * AwaitedRepliesController.h
 *
 *  Created on: 26 Oct 2017
 *      Author: pnikiel
 */

#ifndef INCLUDE_SCA_AWAITEDREPLIESCONTROLLER_H_
#define INCLUDE_SCA_AWAITEDREPLIESCONTROLLER_H_

#include <LogIt.h>

#include <Sca/Request.h>
#include <Sca/Reply.h>
#include <Sca/TransactionIdCoordinator.h>

namespace Sca
{

class AwaitedRepliesController
/***
 * The concept bases on a 256-element array, indexed by transaction IDs, each entry is a "slot".
 * --- assumptions ---
 * - a grouped request can consist of up to 254 requests
 * - a group of requests, when given to this controller, must already have their transaction ids (TIDs) assigned
 * - matching of replies to their requests is based on transaction ids (so the replies can come at any time, in any order)
 * --- example scenario ---
 * Imagine one wants to request 5 transactions on the SCA. They get assigned TIDs: 100, 101, 102, 103, 104.
 * The slot table, stored in this controller, will be initialized to the following vector:
 * 100x UNUSED, 5x EXPECTED, 151x UNUSED.
 * In addition, the slots which get initialized to EXPECTED state, will point (through replyIndex) to 0,1,2,3,4; that is where the replies should start.
 * "stillToGo" counter will be initialized to requested transaction size.
 *
 * When replies start coming, the slots will gradually change state from EXPECTED to RECEIVED, and the stillToGo counter will decrememnt at each such state change.
 * Once the counter is zero, it's clear all replies have been received and the requestor might get notified.
 */
{
public:

    AwaitedRepliesController() :
        m_slots(::Sca::Constants::TRANSACTION_IDS_TOTAL),
        m_replies(0)
    {
        clear();
    }

    enum SlotState { Unused, Expected, Received };
    struct SlotInformation
    {
        SlotState state;
        unsigned char replyIndex;

    };

    void clear()
    {
        // put the whole array to UNUSED state
        m_slots.assign( m_slots.size(), SlotInformation{.state = SlotState::Unused} );
    }

    /*
     * Requirement: the requests given to this function must already have the intended TIDs reserved.
     */
    void prepare (
            std::vector<Request>& requests,
            std::vector<Reply>* replies);



    bool onReceive ( const Reply& reply );

    unsigned int getStillToGo() const { return m_stillToGo; }

private:
    std::vector<SlotInformation> m_slots;
    std::vector<Reply> * m_replies; //TODO: should be const
    unsigned int m_stillToGo;

};

}

#endif /* INCLUDE_SCA_AWAITEDREPLIESCONTROLLER_H_ */
