/*
 * PingSca.cpp
 *
 *  Created on: Aug 31, 2017
 *      Author: pnikiel
 *  Note: the original idea for this program is of Paris Moschovakos
 */

#include <signal.h>
#include <iostream>
#include <sstream>
#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>
#include <Sca/Sca.h>
#include <ScaCommon/except.h>
#include <DemonstratorCommons.h>

template<typename T>
std::string toStringInHex (T v)
{
	std::stringstream ss;
	ss.width(2);
	ss.fill('0');
	ss << std::hex << v;
	return ss.str();
}

struct Config
{
    std::string          hostname;
    unsigned int		 from;
    unsigned int		 to;
    unsigned int         fromHostPort;
    unsigned int         toHostPort;
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
    options_description options;
    std::string logLevelStr;
    std::string fromAsStr, toAsStr;
    options.add_options()
	("help,h", "show help")
	("hostname",      value<std::string>(&config.hostname)->default_value("127.0.0.1"),  "hostname where felixcore is running" ) // in IP address form because netio seems to prefer IPv4
	("from",          value<std::string>(&fromAsStr)->default_value("0"),                "elinks range lower bound - prepend with 0x for hex")
	("to",            value<std::string>(&toAsStr)->default_value("0xff"),               "elinks range upper bound - prepend with 0x for hex")
	("trace_level,t", value<std::string>(&logLevelStr)->default_value("INF"),            "Trace level, one of: ERR,WRN,INF,DBG,TRC")
	("from_host_port",value<unsigned int>(&config.fromHostPort)->default_value(12340),   "FromHost (SCA requests) TCP/IP port of felixccore")
	("to_host_port",  value<unsigned int>(&config.toHostPort)->default_value(12345),     "ToHost (SCA replies) TCP/IP port of felixcore")
	;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
		std::cout << options << std::endl;
		exit(1);
    }
    initializeLogging(logLevelStr);
    config.from = strtol( fromAsStr.c_str(), nullptr, 0);
    config.to = strtol( toAsStr.c_str(), nullptr, 0);
    if (config.from > config.to)
    {
        std::cout << "Given from is higher than given to." << std::endl;
        exit(1);
    }
    return config;
}

bool exitRequired (false);
void handleSigInt (int)
{
	LOG(Log::INF) << "Caught Ctrl-C, clean shutdown will happen soon...";
	exitRequired = true;
}

int main (int argc, char* argv[])
{
	signal(SIGINT, handleSigInt );

	Config config = parseProgramOptions(argc,argv);

	const unsigned int numElinks = config.to - config.from + 1;

	struct ScaInfo
	{
		ScaInfo () { present = false; id = 0x0; }
		bool present;
		uint32_t id;
	};

	std::vector<ScaInfo> foundSca (numElinks, ScaInfo());
	std::cout << "-- scanning begun; it will take ~5 minutes. Summary results will be printed then." << std::endl;
	for (unsigned int elink=config.from; elink <= config.to && !exitRequired; ++elink)
	{
		const std::string address =
		        "simple-netio://direct/" +
		        config.hostname + "/" +
		        boost::lexical_cast<std::string>(config.fromHostPort) + "/" +
		        boost::lexical_cast<std::string>(config.toHostPort) + "/" +
		        toStringInHex (elink);
		try
		{
		    Sca::Sca sca  (address);   // this throws when SCA's not replying at this address
		    foundSca[elink-config.from].present = true;
		    foundSca[elink-config.from].id = sca.getChipId();
		    std::cout << "!! found sca elink=" << toStringInHex(elink) << " ID=" << sca.getChipId() << std::endl;
		}
		catch (Sca::NoReplyException &e)
		{
		    // take it as a fact of life that not every elink has a SCA attached to it. Do nothing.
		}
		catch (std::exception &e)
		{
		    LOG(Log::ERR) << "Exception: " << e.what();
		}
	}
	std::cout << "-- scanning results for hostname " << config.hostname << " -- " << std::endl;
	for (unsigned int elink=config.from; elink <= config.to; ++elink)
		if (foundSca[elink-config.from].present)
			std::cout << "elink " << toStringInHex(elink) << " found SCA, serial=" << foundSca[elink-config.from].id << std::endl;

}


