/**
 * main.cpp
 *
 *  Created on: February 21, 2018
 *      Author: Paris Moschovakos
 *        Note: This diagnostic tool verifies the communication with a 10-bit I2C device
 *        		by faking a 10-bit I2C transaction via GPIO.
 *
 */

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <LogLevels.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <boost/program_options.hpp>
#include <DemonstratorCommons.h>

struct Config
{
    std::string         address;
    double              frequency;
    int			 		sda;
    int					scl;
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
	("help,h", "show help")
	("address,a",     value<std::string>(&config.address)->default_value("sca-simulator://1"),  helpForAddress )
	("frequency,f",   value<double>(&config.frequency)->default_value(1.0/*Hz*/),            "Frequency of polling")
	("SDA,d",   value<int>(&config.sda)->default_value(1),            "GPIO pin for SDA line")
	("SCL,c",   value<int>(&config.scl)->default_value(2),            "GPIO pin for SCL line")
	("trace_level,t", value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
	;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
        std::cout << options << std::endl;
        exit(1);
    }
    initializeLogging(logLevelStr);
    
    return config;
}



int main (int argc, char* argv[])
{
	Config config = parseProgramOptions( argc, argv);
	srand(time(0));

	Sca::Sca sca ( config.address );

	while (1)
	{
		try
		{

		uint32_t regMask, sdaOnly, sclOnly, setBoth, clearBoth = 0;
		regMask ^= (-1 ^ regMask) & (1UL << config.sda);
		regMask ^= (-1 ^ regMask) & (1UL << config.scl);
		LOG(Log::INF) << "SDA is:" << config.sda;
		LOG(Log::INF) << "SCL is:" << config.scl;
		LOG(Log::INF) << "Regmask is: 0x" << std::hex << regMask;

		sdaOnly ^= (-1 ^ sdaOnly) & (1UL << config.sda);
		sclOnly ^= (-1 ^ sclOnly) & (1UL << config.scl);
		setBoth = regMask;

		// Set the direction to the lines that will be used for SDA and SCL and pull them up

		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::Direction, regMask);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, setBoth);

		usleep(1000000.0 / config.frequency );

		LOG(Log::INF) << "[I2C via GPIO] : Is I2C slave ready?";
		char response;
		std::cin >> response;

		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::Direction, regMask);
		// S
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, sclOnly);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, clearBoth);
		// 11110
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, sdaOnly);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, setBoth);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, sdaOnly);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, setBoth);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, sdaOnly);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, setBoth);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, sdaOnly);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, setBoth);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, sdaOnly);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, clearBoth);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, sclOnly);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, clearBoth);
		// Bit9-Bit8 (Now = 0b00)
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, sclOnly);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, clearBoth);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, sclOnly);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, clearBoth);
		// R/W (=0 write) we need a "read" for ROC
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, sclOnly);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, clearBoth);

		// Change direction of SDA to input
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::Direction, sclOnly);

		// Clock out ACK
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, sclOnly);
		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, clearBoth);

		// Print out acknowledge
		auto gpio_datain = sca.gpio().getRegister(::Sca::Constants::GpioRegisters::DataIn);
		LOG(Log::INF) << "Acknowledge value : 0x" << std::hex << gpio_datain;

		}
		catch (std::exception &e)
		{
			LOG(Log::ERR) << "exception " << e.what();
		}
	}

}

