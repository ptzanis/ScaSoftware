/*
 * main.cpp
 *
 *  Created on: Jul 17, 2020
 *      Author: pnikiel
 */

#include <signal.h>

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <LogLevels.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <boost/program_options.hpp>
#include <DemonstratorCommons.h>
#include <thread>

const std::string StyleRed ("\033[1;31m");
const std::string StyleBlue ("\033[1;32m");
const std::string StyleReset ("\033[0m");

#ifdef WITH_EVENT_RECORDER
#include <VeryHipEvents/EventRecorder.h>
using VeryHipEvents::Recorder;
#endif

double subtractTimeval (const timeval &t1, const timeval &t2)
{

    return t2.tv_sec-t1.tv_sec + double(t2.tv_usec-t1.tv_usec)/1000000.0;

}

struct CommonInfo
{
    std::string address;
    double      rate;
    bool        isAlive;
};

struct Config
{
    std::vector<std::string> addresses;
    double                   frequency;
    unsigned int             iterations;
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
	        ("help,h", "show help")
	        ("trace_level,t", value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
	        ("addresses",     value<std::vector<std::string>>(&config.addresses),                       "list of SCA addresses to talk to")
	        ;
    positional_options_description p;
    p.add("addresses", -1);

    variables_map vm;
    store( command_line_parser(argc,argv).options(options).positional(p).run(), vm );
    notify (vm);
    if (vm.count("help"))
    {
        std::cout << options << std::endl;
        exit(1);
    }
    if (vm.count("addresses") < 1)
    {
        LOG(Log::ERR) << "No SCA addresses were specified. Doesn't make sense to run with 0 SCAs. Quitting.";
    }
    initializeLogging(logLevelStr);
    return config;
}

void runner (CommonInfo* info)
{
    std::string address = info->address;
    LOG(Log::INF) << "In the thread for " << address;
    try
    {
        Sca::Sca sca ( address );
        uint32_t id = sca.readChipId();
        info->isAlive = true;
        info->rate = 0;
        LOG(Log::INF) << "Note: SCA " << address << " initialized, id=" << id;
        size_t num_transactions_done (0), total_transactions_done(0);
        timeval last_time, new_time;
        gettimeofday(&last_time, 0);
        while (1)
        {
            sca.readChipId(); // 1 SCA transaction.
            num_transactions_done++;
            gettimeofday(&new_time, 0);
            double timediff = subtractTimeval(last_time, new_time);
            if  (timediff > 2)
            {
                double rate = double(num_transactions_done) / timediff;
                total_transactions_done += num_transactions_done;
                LOG(Log::INF) << "At SCA " << address << " transaction rate is: " << rate << " and total #transactions: " << total_transactions_done;
                num_transactions_done = 0;
                last_time = new_time;
                info->rate = rate;
            }
        }

    }
    catch (const std::exception& e)
    {
        LOG(Log::ERR) << StyleRed << "In the thread of SCA " << address << " exception: " << e.what() << ", terminating this thread." << StyleReset;
    }
    info->isAlive = false;

}

bool exitRequired (false);
void handleSigInt (int)
{
	LOG(Log::INF) << "Caught Ctrl-C, clean shutdown will happen soon...";
	exitRequired = true;
}

int main (int argc, char* argv[])
{
    Config config = parseProgramOptions( argc, argv);
    srand(time(0));

    #ifdef WITH_EVENT_RECORDER
    Recorder::initialize (1E6, 1E8 );
    #endif // WITH_EVENT_RECORDER

    LOG(Log::INF) << "Will try to run with " << config.addresses.size() << " SCAs";

    std::list<std::thread> threads;
    std::list<CommonInfo> commonInfo;

    for (auto addr : config.addresses)
    {
        CommonInfo info;
        info.address = addr;
        commonInfo.push_back(info);
        threads.emplace_back(runner, &commonInfo.back());

    }

    while (!exitRequired)
    {
        usleep (4000000);
        size_t howManyAlive = 0;
        double rate = 0;
        for (auto& info : commonInfo)
        {
            if (info.isAlive)
            {
                howManyAlive++;
                rate += info.rate;
            }

        }
        LOG(Log::INF) << StyleBlue << "Note: " << howManyAlive << " SCAs are still alive and current global (all SCAs) transaction rate is: " << rate << StyleReset;
    }



    #ifdef WITH_EVENT_RECORDER
    Recorder::dump("dump.xml");
    #endif // WITH_EVENT_RECORDER




}
