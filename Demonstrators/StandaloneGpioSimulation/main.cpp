/*
 * main.cpp
 *
 *  Created on: May 3, 2016
 *      Author: aikoulou, based on pnikiel
 */

#include <signal.h>

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <LogLevels.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <boost/program_options.hpp>
#include <DemonstratorCommons.h>

struct Config
{
    std::string          address;
    double               frequency;
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
	("help", "show help")
	("address,a",     value<std::string>(&config.address)->default_value("sca-simulator://1"),  helpForAddress )
	("frequency,f",   value<double>(&config.frequency)->default_value(1.0/*Hz*/),            "Frequency of polling")
	("trace_level,t", value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
	;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
	std::cout << options << std::endl;
	exit(1);
    }
    initializeLogging(logLevelStr);
    return config;
}

bool exitRequired (false);
void handleSigInt (int)
{
	LOG(Log::INF) << "Caught Ctrl-C, clean shutdown will happen soon...";
	exitRequired = true;
}

int main (int argc, char* argv[])
{
	signal(SIGINT, handleSigInt );

	Config config = parseProgramOptions( argc, argv);
	srand(time(0));

	Sca::Sca sca ( config.address ) ;

	sca.gpio().setRegister(::Sca::Constants::GpioRegisters::Direction,0xffffffff);

	while (!exitRequired)
	{
		try
		{
		  	uint32_t test_gpioDataOut;
		  	for (int i =0; i <= 2000; i++)
		  	{
		  		test_gpioDataOut = 0x00000004;
		  		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut,test_gpioDataOut);
				usleep(1000000.0 / config.frequency );

				test_gpioDataOut = 0x00000020;
		  		sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut,test_gpioDataOut);
				usleep(1000000.0 / config.frequency );
		  	}

			auto gpio_dataout = sca.gpio().getRegister(::Sca::Constants::GpioRegisters::DataOut);
			
			LOG(Log::INF) << "[GPIO] expected/obtained :: " << test_gpioDataOut << " / " << gpio_dataout << (test_gpioDataOut==gpio_dataout ? " OK! " : "");

			///reset DIRECTION
			LOG(Log::INF) << "[GPIO] RESET test ... Setting Direction reg to dir out = " << ::Sca::Constants::Gpio::DirectionOut;
			sca.gpio().setRegister(::Sca::Constants::GpioRegisters::Direction,0xffffffff);
		//usleep(1000);
			uint32_t test1 = sca.gpio().getRegisterBitValue(::Sca::Constants::GpioRegisters::Direction,10);
		//usleep(1000);
			uint32_t test2 = sca.gpio().getRegisterBitValue(::Sca::Constants::GpioRegisters::Direction,1);
		//usleep(1000);
			uint32_t test3 = sca.gpio().getRegisterBitValue(::Sca::Constants::GpioRegisters::Direction,21);
		//usleep(1000);
			uint32_t test4 = sca.gpio().getRegisterBitValue(::Sca::Constants::GpioRegisters::Direction,31);
		//usleep(1000);
			if(	test4==::Sca::Constants::Gpio::DirectionIn || 
				test2==::Sca::Constants::Gpio::DirectionIn || 
				test3==::Sca::Constants::Gpio::DirectionIn || 
				test1==::Sca::Constants::Gpio::DirectionIn)
{
				LOG(Log::INF) << "[GPIO] RESET test 1 ..................FAILED? ...  "<< sca.gpio().getRegister(::Sca::Constants::GpioRegisters::Direction);
LOG(Log::INF) << "[GPIO] RESET test ... DirectionIn = " << ::Sca::Constants::Gpio::DirectionIn;
LOG(Log::INF) << "[GPIO] RESET test ... DirectionOut = " << ::Sca::Constants::Gpio::DirectionOut;
LOG(Log::INF) << "[GPIO] RESET test ... test1 = " << test1;
LOG(Log::INF) << "[GPIO] RESET test ... test2 = " << test2;
LOG(Log::INF) << "[GPIO] RESET test ... test3 = " << test3;
LOG(Log::INF) << "[GPIO] RESET test ... test4 = " << test4;

}
else
{
LOG(Log::INF) << "[GPIO] RESET test 1 OK !!!!";
}
			

			//DIRECTION TEST 1
			sca.gpio().setRegisterBitToValue(::Sca::Constants::GpioRegisters::Direction,30,::Sca::Constants::Gpio::DirectionIn);
			uint32_t testDirection1 = sca.gpio().getRegisterBitValue(::Sca::Constants::GpioRegisters::Direction,30);
			
			if( testDirection1 == ::Sca::Constants::Gpio::DirectionIn)
				LOG(Log::INF) << "[GPIO] Direction test 2 OK";
			else
				LOG(Log::INF) << "[GPIO] Direction test 2 ......... FAILED";
				
			///reset DIRECTION
			sca.gpio().setRegisterRangeToValue(::Sca::Constants::GpioRegisters::Direction,1,31,::Sca::Constants::Gpio::DirectionOut);

			uint32_t test5 = sca.gpio().getRegisterBitValue(::Sca::Constants::GpioRegisters::Direction,10);
			uint32_t test6 = sca.gpio().getRegisterBitValue(::Sca::Constants::GpioRegisters::Direction,1);
			uint32_t test7 = sca.gpio().getRegisterBitValue(::Sca::Constants::GpioRegisters::Direction,21);
			uint32_t test8 = sca.gpio().getRegisterBitValue(::Sca::Constants::GpioRegisters::Direction,31);
			if(	test5==::Sca::Constants::Gpio::DirectionIn || 
				test6==::Sca::Constants::Gpio::DirectionIn || 
				test7==::Sca::Constants::Gpio::DirectionIn || 
				test8==::Sca::Constants::Gpio::DirectionIn)
				LOG(Log::INF) << "[GPIO] RESET test 3 ................FAILED?";
			else
				LOG(Log::INF) << "[GPIO] RESET test 3 OK!!";				
			
			/// DIRECTION TEST 2
			sca.gpio().setRegisterRangeToValue(::Sca::Constants::GpioRegisters::Direction,0,31,::Sca::Constants::Gpio::DirectionIn);


			uint32_t testDirection2 = ::Sca::Constants::Gpio::DirectionOut;
			for(int i=0;i<32;i++)
				
			sca.gpio().getRegisterBitValue(::Sca::Constants::GpioRegisters::Direction,30);
			
			if( testDirection2 == ::Sca::Constants::Gpio::DirectionIn)
			{
				LOG(Log::INF) << "[GPIO] Direction test 4 done";
			}

		}
		catch (std::exception &e)
		{
			LOG(Log::ERR) << "exception " << e.what();
		}

		usleep(1000000);
	}






}
