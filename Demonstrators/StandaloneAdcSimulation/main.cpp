/*
 * main.cpp
 *
 *  Created on: May 3, 2016
 *      Author: pnikiel
 */

#include <signal.h>
#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <LogLevels.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <boost/program_options.hpp>
#include <DemonstratorCommons.h>

#ifdef WITH_EVENT_RECORDER
#include <VeryHipEvents/EventRecorder.h>
using VeryHipEvents::Recorder;
#endif

struct Config
{
    std::string          address;
    double               frequency;
    unsigned int         iterations;
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
	        ("help,h", "show help")
	        ("address,a",     value<std::string>(&config.address)->default_value("sca-simulator://1"),   helpForAddress )
	        ("frequency,f",   value<double>(&config.frequency)->default_value(1000.0/*Hz*/),            "Frequency of ADC polling")
	        ("trace_level,t", value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
	        ("iterations,i" , value<unsigned int>(&config.iterations)->
	                default_value(std::numeric_limits<int>::max()),                                     "Number of repetitions" )
	        ;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
        std::cout << options << std::endl;
        exit(1);
    }
    initializeLogging(logLevelStr);
    return config;
}

bool exitRequired (false);
void handleSigInt (int)
{
	LOG(Log::INF) << "Caught Ctrl-C, clean shutdown will happen soon...";
	exitRequired = true;
}

int main (int argc, char* argv[])
{

	signal(SIGINT, handleSigInt );

    Config config = parseProgramOptions( argc, argv);
    srand(time(0));

    #ifdef WITH_EVENT_RECORDER
    Recorder::initialize (1E6, 1E8 );
    #endif // WITH_EVENT_RECORDER

    Sca::Sca sca ( config.address ) ;
    LOG(Log::INF) << "The chip id is: " << std::hex << sca.getChipId();

    unsigned int i=0;
    while (i < config.iterations && !exitRequired)
    {
        try
        {
            for (int i=0; i<32; i++)
            {
                auto value = sca.adc().convertChannel(i);


                LOG(Log::INF) << "ch " <<i << " obtained value " << value;

            }
        }
        catch (std::exception &e)
        {
            LOG(Log::ERR) << "exception " << e.what();
        }

        usleep(1000000.0 / config.frequency );
        ++i;
    }

    #ifdef WITH_EVENT_RECORDER
    Recorder::dump("dump.xml");
    #endif // WITH_EVENT_RECORDER




}
