/**
 * GBTx configuration tool
 *
 *  Created on: March 5, 2018
 *      Author: Paris Moschovakos
 * Description: This standalone program allows to read or configure any GBTx with a
 * 				GBTx configuration file as created by the official GBTx Programmer.
 *
 */

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <LogLevels.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <boost/program_options.hpp>
#include <iomanip>
#include <fstream>
#include <boost/filesystem.hpp>
#include <DemonstratorCommons.h>

using std::vector;

struct Config
{
    std::string         address;
    int					i2cMaster;
    int					frequency;
    bool				sclPadCmosOutput;
    int					i2cDevice;
    std::string			gbtxConfigFile;
    std::string			data;
    int					isRead;

};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
    options_description options("Options");
    std::string logLevelStr;

	options.add_options()
	("help,h", 				"show help")
	("version,v", 			"print version string")
	("address,a",			value<std::string>(&config.address)->default_value("simple-netio://direct/pcatlnswfelix01.cern.ch/12340/12345/08"),  helpForAddress )
	("I2C master,i",		value<int>(&config.i2cMaster), "I2C master [0..15]")
	("I2C device,d",		value<int>(&config.i2cDevice), "I2C device address")
	("read,r",				"read GBTx registers")
	("write,w",				value<std::string>(&config.gbtxConfigFile), "GBTx configuration file path")
	;

    options_description hiddenOptions("Hidden options");
	hiddenOptions.add_options()
	("Trace level,t", 		value<std::string>(&logLevelStr)->default_value("WRN"), "Trace level: ERR,WRN,INF,DBG,TRC")
	("SCL mode,s",			value<bool>(&config.sclPadCmosOutput)->default_value("0"), "SCL mode: open-drain/CMOS output: 0/1")
	("Frequency,f",			value<int>(&config.frequency)->default_value(100), "Bus speed: 100, 200, 400, 1000 (KHz)")
	;

    options_description cmdLineOptions;
    cmdLineOptions.add(options).add(hiddenOptions);

    positional_options_description p;
	p.add("write", -1);

    variables_map vm;
    store( command_line_parser(argc, argv).options(cmdLineOptions).positional(p).run(), vm );
    notify (vm);

    if (vm.count("help"))
    {
	std::cout << options << std::endl;
	exit(1);
    }
    if (vm.count("version"))
    {
    	std::cout << "0.9.20180316" << std::endl;
    	exit(1);
	}
    if ( config.i2cMaster > 15 || config.i2cMaster < 0 )
        {
    	std::cout << "I2C channel requested is out of SCA range: '" << (int)config.i2cMaster << "'" << std::endl;
    	exit(1);
        }
    else
    {
    	config.i2cMaster += Sca::Constants::I2c::I2C_CHANNEL_ID_OFFSET;
    }
    initializeLogging(logLevelStr);
    if (vm.count("read"))
	{
    	config.isRead = 1;
    }
    if (vm.count("write"))
	{
    	config.isRead = 0;
    	boost::filesystem::path p (config.gbtxConfigFile.c_str());
		if ( !(boost::filesystem::exists(p)) ) {
			throw std::runtime_error("The configuration file doesn't exist" );
			exit(1);
		}
	}

    return config;
}

int main (int argc, char* argv[])
{
	Config config = parseProgramOptions(argc, argv);

	uint8_t i2cMaster = config.i2cMaster;
	uint16_t busSpeed = (uint16_t)(config.frequency);
	uint16_t gbtxI2cDeviceAddress = config.i2cDevice;
	bool sclMode = config.sclPadCmosOutput;

	Sca::Sca sca ( config.address );

	try
	{

		uint8_t controlRegister = sca.i2c().getControlRegister( i2cMaster );
		LOG(Log::TRC) << "Initial: Control register: 0x" << std::hex << (int)controlRegister;
		sca.i2c().setFrequency( i2cMaster, busSpeed );
		sca.i2c().setSclMode( i2cMaster, sclMode );
		controlRegister = sca.i2c().getControlRegister( i2cMaster );
		LOG(Log::TRC) << "Reconfigured: Control register: 0x" << std::setbase(16) << (int)controlRegister;

		Sca::I2c::statusRegister statusflags;
		uint8_t greaterThan255 =0;

		/*
		 * GBTx configuration write file
		 *
		 * */
		switch ( config.isRead ) {
		case 0 :
		{
			std::cout << std::setfill('-') << std::setw(120) << "-" <<std::endl;
			std::cout << "Starting GBTx configuration... " << std::endl;
			sca.i2c().setTransmissionByteLength( i2cMaster, Sca::Constants::I2C_NBYTE_3 );
			std::ifstream gbtxConfigFile(config.gbtxConfigFile);
			std::string line;

			for (int i = 0; i <= 365; i++)
			{

				if (i > 255 ) greaterThan255 = 1;

				if (gbtxConfigFile.is_open())
					getline (gbtxConfigFile, line);

				int rawValue = std::stoll(line, 0, 16);

				vector<uint8_t> gbtxInternalRegisterAddressWrite { (uint8_t)(i-greaterThan255*256), greaterThan255, (uint8_t)rawValue } ;

				LOG(Log::TRC) << "Writing 0x" << std::hex << rawValue << " at register " << std::dec << i;
				uint8_t reply = sca.i2c().write( i2cMaster, false, gbtxI2cDeviceAddress, gbtxInternalRegisterAddressWrite);

				statusflags = sca.i2c().getStatus( i2cMaster, reply );
				LOG(Log::TRC) << "successfulTransaction: " << std::boolalpha << statusflags.successfulTransaction;
				LOG(Log::TRC) << "sdaLineLevelError: " << std::boolalpha << statusflags.sdaLineLevelError;
				LOG(Log::TRC) << "invalidCommandSent: " << std::boolalpha << statusflags.invalidCommandSent;
				LOG(Log::TRC) << "lastOperationNotAcknowledged: " << std::boolalpha << statusflags.lastOperationNotAcknowledged;

				if ( !(statusflags.successfulTransaction) )
					throw std::runtime_error("Configuration failed..." );

				std::cout << "\r" << 100*i/365 << "%" << std::flush;

			}
			gbtxConfigFile.close();
			std::cout << "\nGBTx Configuration Done!" << std::endl;
			break;
		}

		/*
		 * GBTx read
		 *
		 * */
		case 1 :
		{
			std::vector<uint8_t> readBackData;
			std::vector<uint8_t> replyRead;
			std::cout << std::setfill('-') << std::setw(120) << "-" <<std::endl;
			std::cout << "Reading GBTx configuration... " << std::endl;

			for (int i = 0; i <= 435; i++)
			{
				if (i > 255 ) greaterThan255 = 1;
				vector<uint8_t> gbtxInternalRegisterAddressRead { (uint8_t)(i-greaterThan255*256), greaterThan255 } ;
				sca.i2c().setTransmissionByteLength( i2cMaster, Sca::Constants::I2C_NBYTE_2 );
				uint8_t reply = sca.i2c().write( i2cMaster, false, gbtxI2cDeviceAddress, gbtxInternalRegisterAddressRead);

				statusflags = sca.i2c().getStatus( i2cMaster, reply );
				LOG(Log::TRC) << "successfulTransaction: " << std::boolalpha << statusflags.successfulTransaction;
				LOG(Log::TRC) << "sdaLineLevelError: " << std::boolalpha << statusflags.sdaLineLevelError;
				LOG(Log::TRC) << "invalidCommandSent: " << std::boolalpha << statusflags.invalidCommandSent;
				LOG(Log::TRC) << "lastOperationNotAcknowledged: " << std::boolalpha << statusflags.lastOperationNotAcknowledged;

				if ( !(statusflags.successfulTransaction) )
					throw std::runtime_error("I2C communication failed..." );

				readBackData = sca.i2c().read( i2cMaster, false, gbtxI2cDeviceAddress, 1);

				statusflags = sca.i2c().getStatus( i2cMaster, readBackData[0] );
				LOG(Log::TRC) << "successfulTransaction: " << std::boolalpha << statusflags.successfulTransaction;
				LOG(Log::TRC) << "sdaLineLevelError: " << std::boolalpha << statusflags.sdaLineLevelError;
				LOG(Log::TRC) << "invalidCommandSent: " << std::boolalpha << statusflags.invalidCommandSent;
				LOG(Log::TRC) << "lastOperationNotAcknowledged: " << std::boolalpha << statusflags.lastOperationNotAcknowledged;

				if ( !(statusflags.successfulTransaction) )
					throw std::runtime_error("I2C communication failed..." );

				std::cout << std::setfill(' ') << std::setw(5) << "Reg " << std::setw(3) << std::dec << i << ": "  <<
						"0x" << std::setw(2) << std::hex << (int)(readBackData[1]) << " ";

				if ( (i+1) % 8 == 0 )
					std::cout << std::endl;
			}

			std::cout << std::endl;
			std::cout << std::setfill('-') << std::setw(120) << "-" <<std::endl;
			LOG(Log::INF) << "Done";
			break;
		}
		default:
			std::cout << "Please append a file to write a GBTx configuration \n";
			break;

		}
	}
	catch (std::exception &e)
	{
		LOG(Log::ERR) << "exception " << e.what();
	}


}
