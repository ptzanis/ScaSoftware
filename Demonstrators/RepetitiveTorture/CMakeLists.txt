link_directories( ${DEMONSTRATORS_COMMON_LIBDIRS} )

add_executable(repetitive_torture 
  main.cpp
  ${DEMONSTRATORS_COMMON_OBJS}
  )

target_link_libraries(repetitive_torture ${DEMONSTRATORS_COMMON_LIBS} )
