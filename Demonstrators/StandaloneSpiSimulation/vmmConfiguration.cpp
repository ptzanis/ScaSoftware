/*
 * main.cpp
 *
 *  Created on: October 21, 2016
 *      Author: Paris Moschovakos
 */

#include <signal.h>

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <LogLevels.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <boost/program_options.hpp>
#include <DemonstratorCommons.h>

using std::vector;

struct Config
{
    std::string          address;
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
	("help,h", "show help")
	("address,a",     value<std::string>(&config.address)->default_value("sca-simulator://1"),  helpForAddress )
	("trace_level,t", value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
	;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
	std::cout << options << std::endl;
	exit(1);
    }
    initializeLogging(logLevelStr);

    return config;
}

bool exitRequired (false);
void handleSigInt (int)
{
	LOG(Log::INF) << "Caught Ctrl-C, clean shutdown will happen soon...";
	exitRequired = true;
}

int main (int argc, char* argv[])
{
	signal(SIGINT, handleSigInt );

	Config config = parseProgramOptions(argc, argv);
	srand(time(0));

	Sca::Sca sca ( config.address ) ;

	while (!exitRequired)
	{
		try
		{

			/* Configure a VMM example */
			vector<uint8_t> vmmConfigurationData
			{//is SOP 0x00
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x04,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x00, 0x00, 0x02, 0x80, 0x34, 0x84, 0xc0, 0x10, 0x22, 0x20
				// last bit is sp (polarity) and it is the last bit to be written to VMM
			};

// Step 1
			sca.spi().setTransmissionBaudRate( 10000000 ); // 20MHz (SCA max speed)
			auto rate = sca.spi().getTransmissionBaudRate();
			LOG(Log::INF) << "Baud rate set at: " << (int)rate << "Hz" ;

// Step 2
			sca.spi().setTransmisionBitLength( 96 );
			auto trLen = sca.spi().getTransmisionBitLength();
			LOG(Log::INF) << "Transmission length set at: " << (int)trLen << "bits" ;

// Step 3: Set ENA to low (0)
			uint32_t directionOut = 0x00000400;
			sca.gpio().setRegister(::Sca::Constants::GpioRegisters::Direction,directionOut);
			uint32_t gpioOutputValue = 0x00000000; //0x00000400 = FFFFFBFF
			sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut,gpioOutputValue);

// Step 4: Set the configuration register - VMM SPI mode is 0
			LOG(Log::INF) << "Setting the control register..." ;

			sca.spi().setSsMode(false); // manual control of slave select

			sca.spi().setTxEdge(false);
			sca.spi().setInvSclk(false);

			sca.spi().setLsbToMsb(false);

			LOG(Log::INF) << "Starting VMM configuration..." ;
			for (int i = 0; i < 1; i++)
			{

// Step 5
				LOG(Log::INF) << "Configuring VMM: " << i ;
				sca.spi().writeSlaves( 0xFF, vmmConfigurationData );

			}

// Step 6: Set ENA to high (1)
			gpioOutputValue = 0x00000400;
			sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut,gpioOutputValue);

// Step 7
			std::cout << "---------------------------" << std::endl ;
			LOG(Log::INF) << "VMM configuration done! " ;
			std::cout << "---------------------------" << std::endl ;

// Step 8 - Read back
			usleep(10000);
			std::vector<uint8_t> replies = sca.spi().getSlaveReplies( 1, 0 );
			std::cout << "Replies acquired" << std::endl ;
			for (auto i : replies) std::cout << std::hex << "0x" << i << " ";
			std::cout << std::endl;

		}
		catch (std::exception &e)
		{
			LOG(Log::ERR) << "exception " << e.what();
		}

		usleep(5000000);
	}




}
