/*
 * Backend.cpp
 *
 *  Created on: 20 Aug 2018
 *      Author: pnikiel
 */

#include <Hdlc/Backend.h>
#include <Hdlc/BackendFactory.h>

namespace Hdlc
{

Backend::~Backend()
{
    BackendFactory::unregister(this);
}

}


