# Quick startup guide

## Get the Demonstrators

For getting the latest demonstrators to ping SCA, read/write GBTx configuration etc you can download them from our release page: 
https://gitlab.cern.ch/atlas-dcs-common-software/ScaSoftware/-/releases

## Build ScaSoftware

For building standalone SCA SW, just use

```
source setup_paths.sh
build_standalone.sh
```

Note that:
1. By default, the software builds with netio and other parts of the TDAQ FELIX Software. 
You can remove netio dependency by changing, in build_standalone, HAVE_NETIO to 0.

2. The easiest way to get netio and TDAQ FELIX is to get the distro from:
https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/apps/

3. Please note that you should use exactly same compiler as the one used for TDAQ FELIX chain. For that source the setup_paths.sh to define your environment.
Don't forget to also source the setup.sh from your FELIX installation directory.
