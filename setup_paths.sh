# This script resolves SCA-SW dependencies from a given LCG release
# and for a specific platform, in order to facilitate the integration 
# with TDAQ software (netIO, felixbase and hdlc_coder)
# Don't forget to source setup.sh from your felix installation directory

# You should source this file, do not execute it!

# Authors: Paris and Piotr

PATH_TO_LCG="/cvmfs/sft.cern.ch/lcg"
LCG_VERSION="LCG_97"
COMPILER="gcc8"
PLATFORM="centos7"
X_BINARY_TAG="x86_64-${PLATFORM}-${COMPILER}-opt"


usage()
{
	echo
	echo "Usage"
	echo
	echo "[-h]: help"
	echo
}

epilogue()
{
	cmake --version
	echo "Dependencies configured. Please also source the setup.sh from your FELIX installation directory."
}
	
grep -e "CentOS" /etc/redhat-release
rc=$?
if [ $rc == 0 ]
then
	echo "We're on CentOS..."
	if [ "$1" == "-h" ]
	then
		usage
	else
		echo "Using ${COMPILER} and ${LCG_VERSION}..."
		#  First the compiler
		if [ -e ${PATH_TO_LCG}/contrib/gcc/8.3.0/${X_BINARY_TAG}/setup.sh ]; then
    		source ${PATH_TO_LCG}/contrib/gcc/8.3.0/${X_BINARY_TAG}/setup.sh
		fi
		#  Then CMake
		if [ -e ${PATH_TO_LCG}/releases/${LCG_VERSION}/CMake/3.14.3/${X_BINARY_TAG}/CMake-env.sh ]; then
			source ${PATH_TO_LCG}/releases/${LCG_VERSION}/CMake/3.14.3/${X_BINARY_TAG}/CMake-env.sh
		fi
		#  Then Boost
		export BOOST_ROOT="${PATH_TO_LCG}/releases/${LCG_VERSION}/Boost/1.72.0/${X_BINARY_TAG}"
		export BOOST_HEADERS="${BOOST_ROOT}/include"
		export BOOST_LIB_DIRECTORIES="${BOOST_ROOT}/lib"  
		export BOOST_LIBS="-lboost_regex -lboost_chrono -lboost_program_options -lboost_thread -lboost_system -lboost_filesystem"
		#  Then protobuf
		export PROTOBUF_HEADERS="${PATH_TO_LCG}/releases/${LCG_VERSION}/protobuf/2.5.0/${X_BINARY_TAG}/include"
		export PROTOBUF_DIRECTORIES="${PATH_TO_LCG}/releases/${LCG_VERSION}/protobuf/2.5.0/${X_BINARY_TAG}/lib"
		export PROTOBUF_LIBS="-lprotobuf"
		epilogue
	fi
fi
