/*
 * ScaSwLogComponents.cpp
 *
 *  Created on: 12 Dec 2018
 *      Author: pnikiel
 */

#include <ScaCommon/ScaSwLogComponents.h>

using namespace Sca;

void LogComponentLevels::initializeScaSw(Log::LOG_LEVEL initialLogLevel)
{
    s_adc = Log::registerLoggingComponent("ScaAdc", initialLogLevel);
    s_dac = Log::registerLoggingComponent("ScaDac", initialLogLevel);
    s_gpio = Log::registerLoggingComponent("ScaGpio", initialLogLevel);
    s_i2c = Log::registerLoggingComponent("ScaI2c", initialLogLevel);
    s_spi = Log::registerLoggingComponent("ScaSpi", initialLogLevel);
    s_jtag = Log::registerLoggingComponent("ScaJtag", initialLogLevel);
    s_sim = Log::registerLoggingComponent("ScaSim", initialLogLevel);
    s_netio = Log::registerLoggingComponent("Netio", initialLogLevel);
    s_ss = Log::registerLoggingComponent("SynchSrv", initialLogLevel);
    s_bef = Log::registerLoggingComponent("BackendFactory", initialLogLevel);
}

Log::LogComponentHandle LogComponentLevels::s_adc = Log::INVALID_HANDLE;
Log::LogComponentHandle LogComponentLevels::s_dac = Log::INVALID_HANDLE;
Log::LogComponentHandle LogComponentLevels::s_gpio = Log::INVALID_HANDLE;
Log::LogComponentHandle LogComponentLevels::s_i2c = Log::INVALID_HANDLE;
Log::LogComponentHandle LogComponentLevels::s_spi = Log::INVALID_HANDLE;
Log::LogComponentHandle LogComponentLevels::s_jtag = Log::INVALID_HANDLE;
Log::LogComponentHandle LogComponentLevels::s_sim = Log::INVALID_HANDLE;
Log::LogComponentHandle LogComponentLevels::s_netio = Log::INVALID_HANDLE;
Log::LogComponentHandle LogComponentLevels::s_ss = Log::INVALID_HANDLE;
Log::LogComponentHandle LogComponentLevels::s_bef = Log::INVALID_HANDLE;
