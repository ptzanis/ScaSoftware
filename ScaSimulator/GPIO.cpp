/*
 * GPIO.cpp
 *
 *  Created on: September 2, 2016
 *      Author: aikoulou, based on pnikiel
 */
#include <ScaSimulator/GPIO.h>

#include <Sca/Defs.h>

#include <stdlib.h>

#include <iostream>

namespace ScaSimulator
{
GPIO::GPIO(unsigned char channelId, HdlcBackend *myBackend) :
		ScaChannel(channelId, myBackend),
		m_DATAOUT(0x00000000),
		m_DATAIN(0x00000000),
		m_DIRECTION(0x00000000),
		m_INTENABLE(0),
		m_INTSEL(0x00000000),
		m_INTTRIG(0x00000000),
		m_CLKSEL(0),
		m_EDGESEL(0x00000000)
{
}
GPIO::~GPIO()
{}
bool GPIO::isGpioReadCommand(uint32_t regCmd)
{
	switch(regCmd)
	{
		case Sca::Constants::Commands::GPIO_R_DATAOUT : return true; break;
		case Sca::Constants::Commands::GPIO_R_DATAIN : return true; break;
		case Sca::Constants::Commands::GPIO_R_DIRECTION : return true; break;
		case Sca::Constants::Commands::GPIO_R_INTENABLE : return true; break;
		case Sca::Constants::Commands::GPIO_R_INTSEL : return true; break;
		case Sca::Constants::Commands::GPIO_R_INTTRIG : return true; break;
		case Sca::Constants::Commands::GPIO_R_CLKSEL : return true; break;
		case Sca::Constants::Commands::GPIO_R_EDGESEL : return true; break;
		default : return false;break;
	}//switch
}//isGpioReadCommand
bool GPIO::isGpioWriteCommand(uint32_t regCmd)
{
	switch(regCmd)
	{
		case Sca::Constants::Commands::GPIO_W_DATAOUT : return true; break;
		case Sca::Constants::Commands::GPIO_W_DIRECTION : return true; break;
		case Sca::Constants::Commands::GPIO_W_INTENABLE : return true; break;
		case Sca::Constants::Commands::GPIO_W_INTSEL : return true; break;
		case Sca::Constants::Commands::GPIO_W_INTTRIG : return true; break;
		case Sca::Constants::Commands::GPIO_W_CLKSEL : return true; break;
		case Sca::Constants::Commands::GPIO_W_EDGESEL : return true; break;
		default : return false;break;
	}//switch
}//isGpioWriteCommand
uint32_t GPIO::getRegisterFromCommand(uint32_t regCmd)
{
	switch(regCmd)
	{
		case Sca::Constants::Commands::GPIO_R_DATAOUT : return m_DATAOUT; break;
		case Sca::Constants::Commands::GPIO_R_DATAIN : return m_DATAIN; break;
		case Sca::Constants::Commands::GPIO_R_DIRECTION : return m_DIRECTION; break;
		case Sca::Constants::Commands::GPIO_R_INTENABLE : return m_INTENABLE; break;
		case Sca::Constants::Commands::GPIO_R_INTSEL : return m_INTSEL; break;
		case Sca::Constants::Commands::GPIO_R_INTTRIG : return m_INTTRIG; break;
		case Sca::Constants::Commands::GPIO_R_CLKSEL : return m_CLKSEL; break;
		case Sca::Constants::Commands::GPIO_R_EDGESEL : return m_EDGESEL; break;
		default : return -1;break;
	}//switch
}//getRegisterFromCommand

void GPIO::setRegisterFromCommand(uint32_t regCmd,uint32_t data)
{

        //std::cout << "Setting data = " << data << "For cmd = "<< regCmd << std::endl;
	switch(regCmd)
	{
		case Sca::Constants::Commands::GPIO_W_DATAOUT :  m_DATAOUT = data; break;
		case Sca::Constants::Commands::GPIO_W_DIRECTION :  m_DIRECTION = data; break;
		case Sca::Constants::Commands::GPIO_W_INTENABLE :  m_INTENABLE = data; break;
		case Sca::Constants::Commands::GPIO_W_INTSEL :  m_INTSEL = data; break;
		case Sca::Constants::Commands::GPIO_W_INTTRIG :  m_INTTRIG = data; break;
		case Sca::Constants::Commands::GPIO_W_CLKSEL :  m_CLKSEL = data; break;
		case Sca::Constants::Commands::GPIO_W_EDGESEL :  m_EDGESEL = data; break;
		default : break;
	}//switch
}//setRegisterFromCommand

void GPIO::sendReadReply(const Sca::Request& request,uint32_t regCmd)
    {
	uint32_t regToSend = getRegisterFromCommand(regCmd);

	Sca::Reply reply (
		Sca::Constants::ChannelIds::GPIO,//channel id
		/*err*/ 0x00,
		/*data = list of 4 bytes*/ {
				(uint8_t)   (regToSend>>24 ),
				(uint8_t)   (regToSend>>16 ),
				(uint8_t)   (regToSend>>8  ),
				(uint8_t)   (regToSend     ) 
				});

    	// Processing time on a GPIO is practically 0
        this->sendReplyLaterKeepBusyFor( request, reply, 0.000001 );

}//sendReadReply

void GPIO::sendWriteReply(const Sca::Request& request,uint32_t regCmd, uint32_t data)
{

	Sca::Reply reply (
		Sca::Constants::ChannelIds::GPIO,//channel id
		/*err*/ 0x00,
		/*data = list of 4 bytes*/ {} );

		// Processing time on a GPIO is practically 0
        this->sendReplyLaterKeepBusyFor( request, reply, 0.000001 );
	
	setRegisterFromCommand(regCmd,data);
		
}//sendWriteReply

void GPIO::onReceive(const Sca::Request &request)
{
	uint32_t cmdOrError = request.command();
	if(isGpioReadCommand(cmdOrError))
		sendReadReply(request, cmdOrError);
	else if(isGpioWriteCommand(cmdOrError))
	{
		uint32_t data = (request[4] << 24) | (request[5] << 16) | (request[6] << 8) | (request[7]);
		sendWriteReply(request, cmdOrError, data);
	}

}//onReceive

}
