/*
 * JTAG.cpp
 *
 *  Created on: February 2, 2017
 *      Author: aikoulou, based on pnikiel
 */
//#include <LogIt.h>
#include <ScaSimulator/JTAG.h>

#include <Sca/Defs.h>

#include <stdlib.h>

#include <iostream>

namespace ScaSimulator
{
JTAG::JTAG(unsigned char channelId, HdlcBackend *myBackend) :
	ScaChannel(channelId, myBackend),
	m_CONTROL(0x1000),
	m_FREQUENCY(0x0) ,
	m_TDO0(0x0)      ,
	m_TDO1(0x0)      ,
	m_TDO2(0x0)      ,
	m_TDO3(0x0)      ,
	m_TDI0(0x0)      ,
	m_TDI1(0x0)      ,
	m_TDI2(0x0)      ,
	m_TDI3(0x0)      ,
	m_TMS0(0x0)      ,  
	m_TMS1(0x0)      ,  
	m_TMS2(0x0)      ,  
	m_TMS3(0x0)        
{}
JTAG::~JTAG()
{}

bool JTAG::isJtagReadCommand(uint32_t regCmd)
{
	switch(regCmd)
	{
		case Sca::Constants::Commands::JTAG_R_CTRL : return true; break;
		case Sca::Constants::Commands::JTAG_R_FREQ : return true; break;
		case Sca::Constants::Commands::JTAG_R_TDI0 : return true; break;
		case Sca::Constants::Commands::JTAG_R_TDI1 : return true; break;
		case Sca::Constants::Commands::JTAG_R_TDI2 : return true; break;
		case Sca::Constants::Commands::JTAG_R_TDI3 : return true; break;
		case Sca::Constants::Commands::JTAG_R_TMS0 : return true; break;
		case Sca::Constants::Commands::JTAG_R_TMS1 : return true; break;
		case Sca::Constants::Commands::JTAG_R_TMS2 : return true; break;
		case Sca::Constants::Commands::JTAG_R_TMS3 : return true; break;
		default : return false;break;
	}//switch
}//isJtagReadCommand
bool JTAG::isJtagWriteCommand(uint32_t regCmd)
{
	switch(regCmd)
	{
		case Sca::Constants::Commands::JTAG_W_CTRL : return true; break;
		case Sca::Constants::Commands::JTAG_W_FREQ : return true; break;
		case Sca::Constants::Commands::JTAG_W_TDO0 : return true; break;
		case Sca::Constants::Commands::JTAG_W_TDO1 : return true; break;
		case Sca::Constants::Commands::JTAG_W_TDO2 : return true; break;
		case Sca::Constants::Commands::JTAG_W_TDO3 : return true; break;
		case Sca::Constants::Commands::JTAG_W_TMS0 : return true; break;
		case Sca::Constants::Commands::JTAG_W_TMS1 : return true; break;
		case Sca::Constants::Commands::JTAG_W_TMS2 : return true; break;
		case Sca::Constants::Commands::JTAG_W_TMS3 : return true; break;
		default : return false;break;
	}//switch
}//isJtagWriteCommand
uint32_t JTAG::getRegisterFromCommand(uint32_t regCmd)
{
	switch(regCmd)
	{
		case Sca::Constants::Commands::JTAG_R_CTRL : return m_CONTROL; break;
		case Sca::Constants::Commands::JTAG_R_FREQ : return m_FREQUENCY; break;
		case Sca::Constants::Commands::JTAG_R_TDI0 : return m_TDI0; break;
		case Sca::Constants::Commands::JTAG_R_TDI1 : return m_TDI1; break;
		case Sca::Constants::Commands::JTAG_R_TDI2 : return m_TDI2; break;
		case Sca::Constants::Commands::JTAG_R_TDI3 : return m_TDI3; break;
		case Sca::Constants::Commands::JTAG_R_TMS0 : return m_TMS0; break;
		case Sca::Constants::Commands::JTAG_R_TMS1 : return m_TMS1; break;
		case Sca::Constants::Commands::JTAG_R_TMS2 : return m_TMS2; break;
		case Sca::Constants::Commands::JTAG_R_TMS3 : return m_TMS3; break;
		default : return false;break;
	}//switch
}//getRegisterFromCommand

void JTAG::setRegisterFromCommand(uint32_t regCmd,uint32_t data)
{
        //std::cout << "Setting data = " << data << "For cmd = "<< regCmd << std::endl;
	switch(regCmd)
	{
		case Sca::Constants::Commands::JTAG_W_CTRL : m_CONTROL = data; break;
		case Sca::Constants::Commands::JTAG_W_FREQ : m_FREQUENCY = data; break;
		case Sca::Constants::Commands::JTAG_W_TDO0 : m_TDO0 = data; break;
		case Sca::Constants::Commands::JTAG_W_TDO1 : m_TDO1 = data; break;
		case Sca::Constants::Commands::JTAG_W_TDO2 : m_TDO2 = data; break;
		case Sca::Constants::Commands::JTAG_W_TDO3 : m_TDO3 = data; break;
		case Sca::Constants::Commands::JTAG_W_TMS0 : m_TMS0 = data; break;
		case Sca::Constants::Commands::JTAG_W_TMS1 : m_TMS1 = data; break;
		case Sca::Constants::Commands::JTAG_W_TMS2 : m_TMS2 = data; break;
		case Sca::Constants::Commands::JTAG_W_TMS3 : m_TMS3 = data; break;
		default : break;
	}//switch
}//setRegisterFromCommand

void JTAG::sendReadReply(const Sca::Request& request,uint32_t regCmd)
    {
	uint32_t regToSend = getRegisterFromCommand(regCmd);

	Sca::Reply reply (
		Sca::Constants::ChannelIds::JTAG,//channel id
		/*err*/ 0x00,
		/*data = list of 4 bytes*/ {
				(uint8_t)   (regToSend>>24 ),
				(uint8_t)   (regToSend>>16 ),
				(uint8_t)   (regToSend>>8  ),
				(uint8_t)   (regToSend     ) 
				});

	this->sendReplyLaterKeepBusyFor( request, reply, double( 1 + rand()%2 )/1E6 );
}//sendReadReply

void JTAG::sendWriteReply(const Sca::Request& request,uint32_t regCmd, uint32_t data)
{

	Sca::Reply reply (
		Sca::Constants::ChannelIds::JTAG,//channel id
		/*err*/ 0x00,
		/*data = list of 4 bytes*/ {0,0,0,0} );

	this->sendReplyLaterKeepBusyFor( request, reply, double( 1 + rand()%2 )/1E6 );
	setRegisterFromCommand(regCmd,data);
		
}//sendWriteReply

void JTAG::onReceive(const Sca::Request &request)
{
	uint32_t cmdOrError = request.command();
	if(isJtagReadCommand(cmdOrError))
		sendReadReply(request, cmdOrError);
	else if(isJtagWriteCommand(cmdOrError))
	{
		uint32_t data = (request[4] << 24) | (request[5] << 16) | (request[6] << 8) | (request[7]);
		sendWriteReply(request, cmdOrError, data);
	}
	else if(cmdOrError == Sca::Constants::Commands::JTAG_ARESET)
	{
		m_CONTROL = 0x1000;
		m_FREQUENCY = 0x0; 
		m_TDO0 = 0x0;      
		m_TDO1 = 0x0;      
		m_TDO2 = 0x0;      
		m_TDO3 = 0x0;      
		m_TDI0 = 0x0;      
		m_TDI1 = 0x0;      
		m_TDI2 = 0x0;      
		m_TDI3 = 0x0;      
		m_TMS0 = 0x0;        
		m_TMS1 = 0x0;        
		m_TMS2 = 0x0;      
		m_TMS3 = 0x0;      
	}//if jtag reset
	else if(cmdOrError == Sca::Constants::Commands::JTAG_GO)
	{
		//"send" data

		//send reply
		Sca::Reply reply (
			Sca::Constants::ChannelIds::JTAG,//channel id
			/*err*/ 0x00,
			/*data = list of 4 bytes*/ {0,0,0,0} );

		this->sendReplyLaterKeepBusyFor( request, reply, double( 1 + rand()%2 )/1E6 );

	}//else if JTAG_GO
	else if(cmdOrError == Sca::Constants::Commands::JTAG_GO_M)
	{
		//"send" data

		//no reply sent (but still must be kept busy)
		unsigned int replyDelay = 1 + rand()%2;

        	boost::chrono::milliseconds processingTime (replyDelay);
        	this->keepBusyFor( processingTime );

	}//else if JTAG_GO_M

}//onReceive

}
