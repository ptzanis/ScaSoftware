/*
 * AdcImplV2.cpp
 *
 *  Created on: May 11, 2016
 *      Author: pnikiel
 */

#include <LogIt.h>

#include <Hdlc/BackendFactory.h>

#include <Sca/Defs.h>
#include <Sca/Sca.h>
#include <Sca/ErrorTranslator.h>

#include <boost/lexical_cast.hpp>

#include <ScaCommon/except.h>
#include <ScaCommon/ScaSwLogComponents.h>
#include <stdexcept>
#include <vector>

using boost::lexical_cast;

namespace Sca
{

uint16_t Adc::convertChannel(uint8_t channelId)
{
    setInputLine (channelId);
    return go();
}

uint16_t Adc::go ()
{
    LOG(Log::TRC, LogComponentLevels::adc()) << "adcGo: request.";
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_GO,
              {0, 0, 1, 0} );
    Reply reply = m_synchronousService.sendAndWaitReply( request );
    throwIfScaReplyError( reply );
    uint16_t value = reply[7]<<8 | reply[6];
    LOG(Log::TRC, LogComponentLevels::adc()) << "adcGo: reply, conversion value=" << value;
    return value;
}

void Adc::setInputLine ( unsigned int inputLine )
{
    LOG(Log::TRC, LogComponentLevels::adc()) << "setInputLine(line=" << inputLine << ")";
    if (inputLine >= ::Sca::Constants::Specs::ADC_CHANNELS_NUM)
        throw std::runtime_error("setInputLine: argument out of range: "+lexical_cast<std::string>(inputLine) );
    // TODO: not sure about bytes mapping: need to be checked
    // TODO: this conforms rather to the old ADC
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_W_INSEL,
              {0, 0, (uint8_t)inputLine, (uint8_t)inputLine} );
    Reply reply = m_synchronousService.sendAndWaitReply( request );
    throwIfScaReplyError( reply );
    LOG(Log::TRC, LogComponentLevels::adc()) << "setInputLine: reply: " << reply.toString();

}

unsigned int Adc::getInputLine ()
{
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_R_INSEL,
              {} );
    // TODO: not sure about bytes mapping: need to be checked
    // TODO: this conforms rather to the old ADC
    Reply reply = m_synchronousService.sendAndWaitReply( request );
    return reply[6];
}

uint32_t Adc::getGainCalibration()
{
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_R_GAIN,
              {} );
    Reply reply = m_synchronousService.sendAndWaitReply( request );
    throwIfScaReplyError(reply);
    return reply[4] << 16 | reply[5] << 24 | reply[6] | reply[7] << 8;
}

// NOTE: it's not understood yet what should be the format of the calibration data. Therefore
// the signature of this function is likely to change soon
void Adc::setGainCalibration(uint32_t x)
{
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_W_GAIN,
              {(uint8_t)(x>>24), (uint8_t)(x>>16), (uint8_t)(x>>8), (uint8_t)x} );
    Reply reply = m_synchronousService.sendAndWaitReply( request );
    throwIfScaReplyError(reply);

}


void Adc::setCurrentSourceMask ( uint32_t mask )
{
    LOG(Log::TRC, LogComponentLevels::adc()) << "setCurrentSourceMask (mask=" << std::hex << mask << ")";
    Request request(
              ::Sca::Constants::ChannelIds::ADC,
              ::Sca::Constants::Commands::ADC_W_CUREN,
              {uint8_t(mask>>16), uint8_t(mask>>24), uint8_t(mask), uint8_t(mask>>8)} );
    Reply reply = m_synchronousService.sendAndWaitReply( request );
    throwIfScaReplyError(reply);
    LOG(Log::TRC, LogComponentLevels::adc()) << "setCurrentSourceMask: reply: " << reply.toString();
}

uint32_t Adc::getCurrentSourceMask ()
{
    LOG(Log::TRC, LogComponentLevels::adc()) << "getCurrentSourceMask ()";
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
            ::Sca::Constants::Commands::ADC_R_CUREN,
            {} );
    Reply reply = m_synchronousService.sendAndWaitReply( request );
    throwIfScaReplyError(reply);

    uint32_t lineBitmap = reply[4] << 16 | reply[5] << 24 | reply[6] | reply[7] << 8;
    LOG(Log::TRC, LogComponentLevels::adc()) << "getCurrentSourceMask: lineBitmap=" << std::hex << lineBitmap;
    return lineBitmap;
}

}
