/*
 * I2cImpl.cpp
 *
 *  Created on: Mar 23, 2017
 *      Author: Paris Moschovakos
 */

#include <LogIt.h>
#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/Sca.h>
#include <Sca/ErrorTranslator.h>
#include <ScaCommon/except.h>
#include <ScaCommon/ScaSwLogComponents.h>
#include <stdexcept>
#include <vector>
#include <bitset>
#include <iomanip>


using boost::lexical_cast;

namespace Sca
{

uint8_t I2c::getControlRegister( uint8_t i2cMaster )
{

	Request request(
			i2cMaster,
	    ::Sca::Constants::Commands::I2C_R_CTRL,
	    {} );

	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Get I2C control register"
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Retrieved I2C control register: " << std::hex << "0x" << std::uppercase <<  (int)reply[5];

	return reply[5];
}

void I2c::setControlRegister ( uint8_t i2cMaster, uint8_t ctrlReg )
{

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Set I2C control register" <<
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Set I2C control register: " << std::hex << "0x" << (int)ctrlReg;

	Request request(
			i2cMaster,
	    ::Sca::Constants::Commands::I2C_W_CTRL,
	    { 0, 0, ctrlReg } );
	throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request ));

}

void I2c::setFrequency ( uint8_t i2cMaster, unsigned int frequency )
{

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Set I2C bus frequency" <<
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Set I2C bus frequency: " << frequency << "kHz";

	// Allowed values are 100, 200, 400 and 1000 in KHz
	uint8_t freqCtrlMask = 0b00;

	switch (frequency) {
		case 100:
			freqCtrlMask = 0b00;
			break;
		case 200:
			freqCtrlMask = 0b01;
			break;
		case 400:
			freqCtrlMask = 0b10;
			break;
		case 1000:
			freqCtrlMask = 0b11;
			break;
		default:
			throw std::invalid_argument( "Frequency is not valid" );
			break;
	}

	m_frequency = frequency;
	uint8_t currentReg = getControlRegister( i2cMaster );
	uint8_t updatedCtrlReg = (freqCtrlMask << 0) | ( currentReg & 0xFC );

	uint8_t data[2] = {0, 0};
	data[1] = updatedCtrlReg;

	Request request(
			i2cMaster,
	    ::Sca::Constants::Commands::I2C_W_CTRL,
		 data, sizeof(data) );
	throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request ));

}

unsigned int I2c::getFrequency ( uint8_t i2cMaster )
{
	auto controlRegister = getControlRegister (i2cMaster);
	auto frequencyCtrlMask = controlRegister & 0x03;

	switch (frequencyCtrlMask) {
		case 0b00:
			m_frequency = ::Sca::Constants::I2C_FREQ_100KHz;
			break;
		case 0b01:
			m_frequency = ::Sca::Constants::I2C_FREQ_200KHz;
			break;
		case 0b10:
			m_frequency = ::Sca::Constants::I2C_FREQ_400KHz;
			break;
		case 0b11:
			m_frequency = ::Sca::Constants::I2C_FREQ_1MHz;
			break;
		default:
			throw std::invalid_argument( "Frequency is not valid" );
			break;
	}

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Get frequency of I2C bus" <<
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Retrieved frequency of I2C bus: " << m_frequency << "kHz";

	return m_frequency;
}

void I2c::setTransmissionByteLength ( uint8_t i2cMaster, uint8_t transmissionByteLength )
{

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Set byte length" <<
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Set byte length: " << (int)transmissionByteLength;

	if ( transmissionByteLength > 16 || transmissionByteLength <= 0 )
	    throw std::out_of_range( "setTransmissionByteLength: argument out of range: " + lexical_cast<std::string>( (int)transmissionByteLength ) );

	m_nbyte = transmissionByteLength;

	uint8_t currentReg = getControlRegister( i2cMaster );
	uint8_t updatedCtrlReg = (transmissionByteLength << 2) | ( currentReg & 0x83 );

	Request request(
			i2cMaster,
			::Sca::Constants::Commands::I2C_W_CTRL,
			 { 0, updatedCtrlReg, 0, 0 } );
	throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request ));

}

void I2c::setSclMode ( uint8_t i2cMaster, bool sclMode )
{

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Set SCL mode"
			", I2C Master: " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", SCL pad act as CMOS output: " << std::boolalpha << sclMode;

	m_sclmode = sclMode;

	uint8_t currentReg = getControlRegister( i2cMaster );

	uint8_t updatedCtrlReg = (sclMode << 7) | ( currentReg & 0x7f );

	Request request(
			i2cMaster,
	    ::Sca::Constants::Commands::I2C_W_CTRL,
	    { 0, updatedCtrlReg, 0, 0 } );
	throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request ));

}

bool I2c::getSclMode ( uint8_t i2cMaster )
{

	m_sclmode = std::bitset<8>(getControlRegister (i2cMaster)).test(7);

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Get SCL mode"
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Is SCL mode in CMOS output mode: " << std::boolalpha << m_sclmode;

	return m_sclmode;
}

uint8_t I2c::getStatusRegister( uint8_t i2cMaster )
{

	m_lasti2cMaster = i2cMaster;

	Request request(
			i2cMaster,
	    ::Sca::Constants::Commands::I2C_R_STR,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Get status register"
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Get I2C status register: " << std::hex << "0x" << (int)reply[5];

	return reply[5] ;
}

I2c::statusRegister I2c::getStatus( uint8_t i2cMaster, uint8_t statusReg )
{

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Get status"
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Status register contents: " << std::hex << "0x" << (int)statusReg;

	if ( statusReg == 255 )
		statusReg = getStatusRegister( i2cMaster );

	statusRegister statusRegisterContents;

	statusRegisterContents.successfulTransaction = ( statusReg >> 2 ) & 1;
	statusRegisterContents.sdaLineLevelError = ( statusReg >> 3 ) & 1;
	statusRegisterContents.invalidCommandSent = ( statusReg >> 5 ) & 1;
	statusRegisterContents.lastOperationNotAcknowledged = ( statusReg >> 6 ) & 1;

	return statusRegisterContents;
}

void I2c::setDataRegister ( uint8_t i2cMaster, std::vector<uint8_t> &data )
{

	if ( data.size() > 16 || data.size() < 1 )
		throw std::runtime_error("Data size is not allowed: " + lexical_cast<std::string>( data.size() ) + " byte(s)" );

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Set data register"
			", I2C Master " <<	(int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Set data: " << vectorToHexString( data );

    if ( data.size() > 12 )
    {

        Request request4(
        		i2cMaster,
        	::Sca::Constants::Commands::I2C_W_DATA3,
			 { data[13], data[12], data[15], data[14] } );
        throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request4 ));
    }

    if ( data.size() > 8 )
    {
        Request request3(
        		i2cMaster,
        	::Sca::Constants::Commands::I2C_W_DATA2,
        	{ data[9], data[8], data[11], data[10] } );
        throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request3 ));

    }

    if ( data.size() > 4 )
	{
        Request request2(
        		i2cMaster,
        	::Sca::Constants::Commands::I2C_W_DATA1,
        	{ data[5], data[4], data[7], data[6] } );
        throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request2 ));

	}

    Request request1(
    		i2cMaster,
    	::Sca::Constants::Commands::I2C_W_DATA0,
		 { data[1], data[0], data[3], data[2] } );
    throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request1 ));

}

std::vector<uint8_t> I2c::getDataRegister ( uint8_t i2cMaster )
{

    Request request1(
    		i2cMaster,
			::Sca::Constants::Commands::I2C_R_DATA3,
			 {} );
    Reply reply1 = m_synchronousService.sendAndWaitReply( request1 );
    throwIfScaReplyError(reply1);

    std::vector<uint8_t> reply{reply1[5], reply1[4], reply1[7], reply1[6]};

    if ( m_nbyte > 4 )
	{
        Request request2(
        		i2cMaster,
				::Sca::Constants::Commands::I2C_R_DATA2,
				 {} );
        Reply reply2 = m_synchronousService.sendAndWaitReply( request2 );
        throwIfScaReplyError(reply2);

        reply.push_back(reply2[5]);
        reply.push_back(reply2[4]);
        reply.push_back(reply2[7]);
        reply.push_back(reply2[6]);

	}

    if ( m_nbyte > 8 )
    {
        Request request3(
        		i2cMaster,
				::Sca::Constants::Commands::I2C_R_DATA1,
				 {} );
        Reply reply3 = m_synchronousService.sendAndWaitReply( request3 );
        throwIfScaReplyError(reply3);

        reply.push_back(reply3[5]);
        reply.push_back(reply3[4]);
        reply.push_back(reply3[7]);
        reply.push_back(reply3[6]);
    }

    if ( m_nbyte > 12 )
    {

        Request request4(
        		i2cMaster,
				::Sca::Constants::Commands::I2C_R_DATA0,
				 {} );
        Reply reply4 = m_synchronousService.sendAndWaitReply( request4 );
        throwIfScaReplyError(reply4);

        reply.push_back(reply4[5]);
        reply.push_back(reply4[4]);
        reply.push_back(reply4[7]);
        reply.push_back(reply4[6]);
    }

    LOG(Log::TRC, LogComponentLevels::i2c()) << "Get data register" <<
    		", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Get data register: " << vectorToHexString( reply );

    return reply;
}

std::vector<uint8_t> I2c::readSingleByte7bit ( uint8_t i2cMaster, uint8_t address )
{

	if ( address > 127 || address < 0 )
	    throw std::out_of_range( "I2C register address: argument out of range: " + address );

	setTransmissionByteLength( i2cMaster, ::Sca::Constants::I2c::I2C_NBYTE_1 );

	uint8_t formattedAddressD0 = address;

	Request request(
			i2cMaster,
	    	::Sca::Constants::Commands::I2C_S_7B_R,
			 { 0, formattedAddressD0, 0, 0 } );

	Reply reply1 = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply1);

	std::vector<uint8_t> reply{reply1[5], reply1[4]};

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Read single byte 7-bit" <<
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Retrieved data: 0x" << std::hex << (int)reply1[5] << " 0x" << reply1[4];

	return reply;
}

uint8_t I2c::writeSingleByte7bit ( uint8_t i2cMaster, uint8_t address, uint8_t data )
{

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Write single byte 7-bit" <<
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Address: 0x" << std::hex << (int)address <<
			", Data: 0x" << (int)data;

	uint8_t formattedaddressD0 = address;

    Request request(
    		i2cMaster,
    	::Sca::Constants::Commands::I2C_S_7B_W,
		 { data, formattedaddressD0, 0, 0 } );

    Reply reply = m_synchronousService.sendAndWaitReply( request );
    throwIfScaReplyError(reply);

    return reply[5];

}

std::vector<uint8_t> I2c::readMultiByte7bit ( uint8_t i2cMaster, uint8_t address, uint8_t nbytes )
{

	if ( address > 127 || address < 0 )
	    throw std::out_of_range( "I2C register address: argument out of range: " + address );

	uint8_t formattedAddressD0 = address; // We dont have control over the R/W bit

	Request request(
			i2cMaster,
	    	::Sca::Constants::Commands::I2C_M_7B_R,
			 { 0, formattedAddressD0, 0, 0 } );
	Reply reply1 = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply1);

	std::vector<uint8_t> reply{reply1[5]};
	std::vector<uint8_t> data = getDataRegister( i2cMaster );

	reply.insert(std::end(reply), std::begin(data), std::end(data));

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Read multi-byte 7bit"
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Read multi-byte 7 bit mode: " << std::hex << "Address: 0x" << (int)address <<
			", Number of bytes to read: " << (int)nbytes <<
			", Retrieved data: " << vectorToHexString( reply );

	return reply;
}

uint8_t I2c::writeMultiByte7bit ( uint8_t i2cMaster, uint8_t address, std::vector<uint8_t> &data )
{

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Write multi-byte 7 bit mode"
			", I2C Master: " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Address: 0x" << std::hex << (int)address <<
			", Data: " << vectorToHexString( data );

	setDataRegister( i2cMaster, data );

    Request request(
    		i2cMaster,
    	::Sca::Constants::Commands::I2C_M_7B_W,
		 { 0, address, 0, 0 } );

    Reply reply = m_synchronousService.sendAndWaitReply( request );
    throwIfScaReplyError(reply);

    return reply[5];

}

std::vector<uint8_t> I2c::readSingleByte10bit ( uint8_t i2cMaster, uint16_t address )
{

	if ( address > 1023 || address < 0 )
	    throw std::out_of_range( "I2C register address: argument out of range: " + address );

	setTransmissionByteLength( i2cMaster, ::Sca::Constants::I2c::I2C_NBYTE_1 );

	uint8_t formattedAddressD0 = ::Sca::Constants::I2c::I2C_10B_ADDRESS_OFFSET + ( address >> 8 );
	uint8_t formattedaddressD1 = (uint8_t)address;

	Request request(
			i2cMaster,
	    	::Sca::Constants::Commands::I2C_S_10B_R,
			 { formattedaddressD1, formattedAddressD0, 0, 0 } );

	Reply reply1 = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply1);

	std::vector<uint8_t> reply{reply1[5], reply1[4]};

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Read single Byte 10-bit"
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", Address: 0x" << std::hex << (int)address <<
			", Retrieved data: 0x" << (int)reply1[5] << " 0x" << (int)reply1[4];

	return reply;
}

uint8_t I2c::writeSingleByte10bit ( uint8_t i2cMaster, uint16_t address, uint8_t data )
{

	LOG(Log::TRC, LogComponentLevels::i2c()) << "I2C Master " <<
				(int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
				": Write single byte 10 bit mode: " << std::hex << "Address: 0x" << (int)address <<
				" Data: 0x" << std::hex << (int)data;

	uint8_t formattedAddressD0 = ::Sca::Constants::I2c::I2C_10B_ADDRESS_OFFSET + ( address >> 8 );
	uint8_t formattedaddressD1 = (uint8_t)address;

    Request request(
    		i2cMaster,
    	::Sca::Constants::Commands::I2C_S_10B_W,
		 { formattedaddressD1, formattedAddressD0, 0, data } );

    Reply reply = m_synchronousService.sendAndWaitReply( request );
    throwIfScaReplyError(reply);

    return reply[5];

}

std::vector<uint8_t> I2c::readMultiByte10bit ( uint8_t i2cMaster, uint16_t address, uint8_t nbytes )
{

	if ( address > 1023 || address < 0 )
	    throw std::out_of_range( "I2C register address: argument out of range: " + address );

	uint8_t formattedAddressD0 = ::Sca::Constants::I2c::I2C_10B_ADDRESS_OFFSET + ( address >> 8 );
	uint8_t formattedaddressD1 = (uint8_t)address;

	Request request(
			i2cMaster,
	    	::Sca::Constants::Commands::I2C_M_10B_R,
			 { formattedaddressD1, formattedAddressD0, 0, 0 } );
	Reply reply1 = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply1);

	std::vector<uint8_t> reply{reply1[5]};
	std::vector<uint8_t> data = getDataRegister( i2cMaster );

	reply.insert(std::end(reply), std::begin(data), std::end(data));

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Read multi-byte 10 bit mode:" <<
			" I2C Master: " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			" Address: 0x" << std::hex << (int)address <<
			" Number of bytes to read: " << std::dec << (int)nbytes <<
			" Retrieved data: " << vectorToHexString( reply );

	return reply;
}

uint8_t I2c::writeMultiByte10bit ( uint8_t i2cMaster, uint16_t address, std::vector<uint8_t> &data )
{

	LOG(Log::TRC, LogComponentLevels::i2c()) << "Write multi-byte 10 bit mode:"
			" I2C Master: " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			" Address: 0x" << std::hex << (int)address <<
			" Data: " << vectorToHexString( data );
			std::for_each( data.cbegin(), data.cend(), [] (const char c) {std::cout << c << " ";} );

	setDataRegister( i2cMaster, data );

	uint8_t formattedAddressD0 = ::Sca::Constants::I2c::I2C_10B_ADDRESS_OFFSET + ( address >> 8 );
	uint8_t formattedaddressD1 = (uint8_t)address;

    Request request(
    		i2cMaster,
    	::Sca::Constants::Commands::I2C_M_10B_W,
		 { formattedaddressD1, formattedAddressD0, 0, 0 } );

    Reply reply = m_synchronousService.sendAndWaitReply( request );
    throwIfScaReplyError(reply);

    return reply[5];

}

uint8_t I2c::write ( uint8_t i2cMaster, bool addressingMode, uint16_t address, std::vector<uint8_t> &data )
{

	LOG(Log::TRC, LogComponentLevels::i2c()) << "General write request"
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", 10-bit addressing mode: " << std::boolalpha << addressingMode <<
			", Address: 0x" << std::hex << (int)address <<
			", Data to write: " << vectorToHexString( data );

	if ( data.size() > 16 || data.size() <= 0 )
	    throw std::out_of_range( "I2C write: amount of data out of range: " );

	setTransmissionByteLength( i2cMaster, (uint8_t)(data.size()) );

	uint8_t status;

	if (addressingMode)
	{
		status = data.size() > 1 ? writeMultiByte10bit( i2cMaster, address, data ) : writeSingleByte10bit( i2cMaster, address, data[0] );
	}
	else
	{
		uint8_t address8 = (uint8_t)address;
		status = data.size() > 1 ? writeMultiByte7bit( i2cMaster, address8, data ) : writeSingleByte7bit( i2cMaster, address8, data[0] );
	}

    return status;

}

std::vector<uint8_t> I2c::read ( uint8_t i2cMaster, bool addressingMode, uint16_t address, uint8_t nbytes )
{

	setTransmissionByteLength( i2cMaster, nbytes );
	std::vector<uint8_t> statusData;

	if (addressingMode)
	{
		statusData = nbytes > 1 ? readMultiByte10bit( i2cMaster, address, nbytes ) : readSingleByte10bit( i2cMaster, address );
	}
	else
	{
		uint8_t address8 = (uint8_t)address;
		statusData = nbytes > 1 ? readMultiByte7bit( i2cMaster, address8, nbytes ) : readSingleByte7bit( i2cMaster, address8 );
	}

	LOG(Log::TRC, LogComponentLevels::i2c()) << "General read request"
			", I2C Master " << (int)i2cMaster - ::Sca::Constants::I2C_CHANNEL_ID_OFFSET <<
			", 10-bit addressing mode: " << std::boolalpha << addressingMode <<
			", Address: 0x" << std::hex << (int)address <<
			", Requested number of bytes: " << std::dec << (int)nbytes <<
			", Retrieved data: " << vectorToHexString(statusData);

    return statusData;

}

std::string I2c::vectorToHexString( const std::vector<uint8_t>& data )
{
	std::ostringstream ss;

	ss << std::hex << std::uppercase;
	std::for_each( data.cbegin(), data.cend(), [&]( int c ) { ss << std::setw( 3 ) << "0x" << c; } );
	std::string result = ss.str();

	return result;
}

} /* namespace Sca */

