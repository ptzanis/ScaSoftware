/*
 * SynchronousService.cpp
 *
 *  Created on: May 9, 2016
 *      Author: pnikiel
 */

#include <Sca/SynchronousService.h>
#include <Sca/Reply.h>
#include <Sca/ScaSwVeryHipEventTypes.h>

#include <Hdlc/Backend.h>

#include <ScaCommon/except.h>
#include <ScaCommon/ScaSwLogComponents.h>

#include <LogIt.h>

#include <boost/chrono.hpp>
#include <boost/bind.hpp>

#include <algorithm>

using Hdlc::Payload;

namespace Sca
{

SynchronousService::SynchronousService( ::Hdlc::Backend * backend ):
		m_backend( backend ),
		m_lostRepliesCounter(0)
{
	m_backend->subscribeReceiver( boost::bind(&SynchronousService::onReceive, this, _1) );

}

SynchronousService::~SynchronousService()
{
}

void SynchronousService::registerChannel(unsigned char channelId)
{
	// TODO: what if the channel is already registered
	m_channelMap.insert( std::pair<unsigned char, SynchronousChannel*>(channelId, new SynchronousChannel (m_backend, *this) ) );
}

Reply SynchronousService::sendAndWaitReply(
		Request& request,
		unsigned int timeoutMs)
{
	auto channelId = request.channelId();
	// get channel bound to this channelId -- at[] throws if no channel so this is safe
	SynchronousChannel * channel = m_channelMap.at( channelId );
	return channel->sendAndWaitReply( request, timeoutMs );
}

std::vector<Reply> SynchronousService::sendAndWaitReply (
        std::vector<Request> &requests,
        unsigned int timeoutMs )
{
    if (requests.size() < 1)
        scasw_throw_runtime_error_with_origin("logic-error: sendAndWaitReply called with empty vector of requests");
    unsigned int supposedChannelId = requests[0].channelId();

    if (! std::all_of(requests.begin(), requests.end(), [supposedChannelId]( const Request& r){ return r.channelId() == supposedChannelId; } ))
        scasw_throw_runtime_error_with_origin("the grouped request to sendAndWaitReply must be uniform with regard to channel number");

    SynchronousChannel * channel = m_channelMap.at( supposedChannelId );

    return channel->sendAndWaitReply( requests, timeoutMs );
}


Reply Sca::SynchronousService::SynchronousChannel::sendAndWaitReply(
		Request& request,
		unsigned int timeoutMs)
{
    std::vector<Request> requests = {request};
    std::vector<Reply> replies = this->sendAndWaitReply( requests, timeoutMs );
    return replies[0];
}

std::vector<Reply> Sca::SynchronousService::SynchronousChannel::sendAndWaitReply(
        std::vector<Request>& requests,
        unsigned int timeoutMs)
{
    if (requests.size()>::Sca::Constants::Specs::TRANSACTION_IDS_AVAILABLE)
        scasw_throw_runtime_error_with_origin("illegal to request a group of transactions bigger than" + std::to_string(::Sca::Constants::Specs::TRANSACTION_IDS_AVAILABLE) + "transactions for the same SCA component");
    std::vector<Reply> replies ( requests.size(), /* place-holder for factual reply*/ Reply (0, 0, {}) );

    // make sure there is only one user per channel
    boost::system_time returnUntil ( boost::get_system_time() + boost::posix_time::milliseconds(timeoutMs) );
    boost::unique_lock<decltype(m_oneUserPerChannelLock)> lock( m_oneUserPerChannelLock, returnUntil );
    if (! lock.owns_lock() )
        scasw_throw_runtime_error_with_origin("failed to obtain channel lock within permitted time");

    // allocate transaction ids
    for (Request& request : requests)
    {
        unsigned int tid = 0;
        if (! m_tidCoordinator.allocateTid( &tid ))
        {
            // couldn't allocate more TID's, free those which are already requested.
            std::for_each(
                    requests.begin(),
                    requests.end(),
                    [ this ]( Request &r ) { m_tidCoordinator.freeTid (r.transactionId()); }
            );
            scasw_throw_runtime_error_with_origin("all transaction ids in use!");
        }
        request.assignTransactionId( tid );
    }

    #ifdef WITH_EVENT_RECORDER
    record_event<SendEvent>( requests.front().transactionId(), requests.back().transactionId() );
    #endif

    // STL transform code
    std::vector<Payload> payloads ( requests.size(), /*this is a placeholder payload*/ Payload(0,0) );
    std::transform(
            requests.begin(),
            requests.end(),
            payloads.begin(),
            [](Request& request){ return request; }
            );

    std::vector<unsigned int> times (requests.size() );
    std::transform(
            requests.begin(),
            requests.end(),
            times.begin(),
            [](Request& request){ return request.trailingPause(); }
            );

    // prepare condition variable
    boost::unique_lock<boost::mutex> lockCondVar (m_condVarLock);

    m_awaitedRepliesController.prepare(
            requests,
            &replies );

    // send requests
    m_backend->send(
            payloads,
            times);

    bool gotNotification = false;

    gotNotification = m_condVar.timed_wait( lockCondVar, returnUntil );

    std::for_each(
            requests.begin(),
            requests.end(),
            [ this ]( Request &r ) { m_tidCoordinator.freeTid (r.transactionId()); }
    );

    if (!gotNotification)
    {
        m_awaitedRepliesController.clear();
        m_synchronousService.onLostReplies(m_awaitedRepliesController.getStillToGo());
        THROW_WITH_ORIGIN(NoReplyException,
                "At SCA with address: '" + m_backend->getAddress() + "' " +
                "reply hasnt come, #requests="+
                boost::lexical_cast<std::string>(requests.size())+
                " #received="+
                boost::lexical_cast<std::string>(requests.size()-m_awaitedRepliesController.getStillToGo()));
    }
    return replies;

}
void SynchronousService::onReceive(const ::Hdlc::Payload& hdlcReply)
{
	Reply reply (hdlcReply);

    #ifdef WITH_EVENT_RECORDER
	record_event<ReceiveEvent>( reply.transactionId() );
    #endif

	LOG(Log::TRC, LogComponentLevels::ss()) <<
	        "At SCA with address: '" <<
	        m_backend->getAddress() << "' " <<
	        "obtained reply: " << reply.toString();
	unsigned char channelId = reply.channelId ();
	try
	{
		SynchronousChannel * channel = m_channelMap.at( channelId );
		channel->onReceive( reply );
	}
	catch (std::out_of_range &e)
	{
		LOG(Log::ERR, LogComponentLevels::ss()) <<
	            "At SCA with address: '" <<
	            m_backend->getAddress() << "' " <<
		        "obtained reply without prior request (to unregistered channel!)";
	}
}

void SynchronousService::SynchronousChannel::onReceive(const ::Hdlc::Payload& reply)
{
	boost::lock_guard<boost::mutex> lock(m_condVarLock);

	bool requestComplete = m_awaitedRepliesController.onReceive( reply );

	if (requestComplete)
	    m_condVar.notify_one();
}

}


