/*
 * GpioImpl.cpp
 *
 *  Created on: Mar 23, 2017
 *      Author: Aimilios Koulouris, Paris Moschovakos
 */

#include <LogIt.h>

#include <Hdlc/BackendFactory.h>

#include <Sca/Defs.h>
#include <Sca/Sca.h>

#include <boost/lexical_cast.hpp>

#include <ScaCommon/except.h>
#include <ScaCommon/ScaSwLogComponents.h>

#include <stdexcept>
#include <vector>
#include <fstream>
#include <iterator>


using boost::lexical_cast;
namespace Sca
{

/*
* GPIO COMMANDS
*/

/*
* LOW LEVEL
*/

// This function is used to getReadCommandFromRegister.
// Instead of having a separate command for each register, the user will use more general commands which in turn use this function
uint32_t Gpio::getReadCommandFromRegister(uint32_t gpioRegister)
{
	switch(gpioRegister)
	{
	case ::Sca::Constants::GpioRegisters::DataOut: 		return ::Sca::Constants::Commands::GPIO_R_DATAOUT; break;
	case ::Sca::Constants::GpioRegisters::DataIn : 		return ::Sca::Constants::Commands::GPIO_R_DATAIN; break;
	case ::Sca::Constants::GpioRegisters::Direction : 	return ::Sca::Constants::Commands::GPIO_R_DIRECTION; break;
	case ::Sca::Constants::GpioRegisters::InterruptSelect :	return ::Sca::Constants::Commands::GPIO_R_INTSEL; break;
	case ::Sca::Constants::GpioRegisters::InterruptTrigger :return ::Sca::Constants::Commands::GPIO_R_INTTRIG; break;
	case ::Sca::Constants::GpioRegisters::EdgeSelect :	return ::Sca::Constants::Commands::GPIO_R_EDGESEL; break;
	case ::Sca::Constants::GpioFlags::InterruptEnable :	return ::Sca::Constants::Commands::GPIO_R_INTENABLE; break;
	case ::Sca::Constants::GpioFlags::ClockSelect : 	return ::Sca::Constants::Commands::GPIO_R_CLKSEL; break;
	default: throw std::runtime_error("[GPIO] invalid register input"); break;
	}
}
// This function is used to getWriteCommandFromRegister.
// Instead of having a separate command for each register, the user will use more general commands which in turn use this function
uint32_t Gpio::getWriteCommandFromRegister(uint32_t gpioRegister)
{
	switch(gpioRegister)
	{
	case ::Sca::Constants::GpioRegisters::DataOut: 		return ::Sca::Constants::Commands::GPIO_W_DATAOUT; break;
	case ::Sca::Constants::GpioRegisters::Direction : 	return ::Sca::Constants::Commands::GPIO_W_DIRECTION; break;
	case ::Sca::Constants::GpioRegisters::InterruptSelect :	return ::Sca::Constants::Commands::GPIO_W_INTSEL; break;
	case ::Sca::Constants::GpioRegisters::InterruptTrigger :return ::Sca::Constants::Commands::GPIO_W_INTTRIG; break;
	case ::Sca::Constants::GpioRegisters::EdgeSelect :	return ::Sca::Constants::Commands::GPIO_W_EDGESEL; break;
	case ::Sca::Constants::GpioFlags::InterruptEnable :	return ::Sca::Constants::Commands::GPIO_W_INTENABLE; break;
	case ::Sca::Constants::GpioFlags::ClockSelect : 	return ::Sca::Constants::Commands::GPIO_W_CLKSEL; break;
	default: throw std::runtime_error("[GPIO] invalid register input"); break;
	}
}
//This is the prototype method for sending a WRITE command
void Gpio::sendWriteDataToRegister(uint32_t data, uint32_t gpioRegister)
{
	Request request(
			::Sca::Constants::ChannelIds::GPIO,
			getWriteCommandFromRegister(gpioRegister),
			// NOTE: the order below has been validated by Piotr with the SCA digital daughterboard 13-07-2017
			{ (uint8_t) (data >> 16),  
			  (uint8_t) (data >> 24),  
			  (uint8_t) (data >>  0),  
			  (uint8_t) (data >>  8)   
			}
			 );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);
}
//This is the prototype method for sending a READ command
uint32_t Gpio::sendReadRegister(uint32_t gpioRegister)
{
	Request request(
			::Sca::Constants::ChannelIds::GPIO,
			getReadCommandFromRegister(gpioRegister),
			{}
			 );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	
	throwIfScaReplyError(reply);
	// NOTE: the order below has been validated by Piotr with the SCA digital daughterboard 13-07-2017
	return reply[5]<<24 | reply[4]<<16 | reply[7]<<8 | reply[6];
}
/*
*	USER LEVEL
*/
//The user can call:
// >my_sca.gpio().getRegister(::Sca::Constants::GpioRegisters::DataOut)
//to read DataOut
// Or the user can call:
// >my_sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, 0xC0CAC01A)
//to write the 32-bit word C0CAC01A to register DataOut
uint32_t Gpio::getRegister(uint32_t gpioRegister)
{
	LOG(Log::TRC, LogComponentLevels::gpio()) << "Reading register: " << std::hex << gpioRegister;

	return sendReadRegister(gpioRegister);
}
void Gpio::setRegister(uint32_t gpioRegister, uint32_t data)
{
	LOG(Log::TRC, LogComponentLevels::gpio()) << " reg= " << std::hex << gpioRegister << " val= " << data;

	return sendWriteDataToRegister(data, gpioRegister);
}
bool Gpio::getRegisterBitValue(uint32_t gpioRegister, int pinIndex)
{
	LOG(Log::TRC, LogComponentLevels::gpio()) << "Reading pin: "<<pinIndex<<" from register: " << std::hex << gpioRegister;

	if(gpioRegister==::Sca::Constants::GpioFlags::InterruptEnable || gpioRegister==::Sca::Constants::GpioFlags::ClockSelect)
		scasw_throw_runtime_error_with_origin("Cannot use this command on flag register");

	if(pinIndex<0 || pinIndex>31) scasw_throw_runtime_error_with_origin("Invalid Pin number");

	return (  getRegister(gpioRegister)  ) & (1<<pinIndex);
}

void Gpio::setRegisterBitToValue(uint32_t gpioRegister, int pinIndex, bool setValue)
{
	LOG(Log::TRC, LogComponentLevels::gpio()) << "Setting pin: "<<pinIndex<<" of register: " << std::hex << gpioRegister<<" to: "<<(setValue ? 1 : 0);

	if(gpioRegister==::Sca::Constants::GpioFlags::InterruptEnable || gpioRegister==::Sca::Constants::GpioFlags::ClockSelect)
		scasw_throw_runtime_error_with_origin("Cannot use this command on flag register");

	if(pinIndex<0 || pinIndex>31) scasw_throw_runtime_error_with_origin("Invalid Pin number");
	uint32_t my_register = getRegister(gpioRegister);
	my_register &= ~(1<<pinIndex); //clears the pinIndex-th pin
	if(setValue)
		my_register |= 1<<pinIndex; //if we were to set it to 1, now it will be set to 1
	setRegister(gpioRegister,my_register);
}
std::vector<bool> Gpio::getRegisterRangeValue(uint32_t gpioRegister, int fromPin, int toPin)
{
	LOG(Log::TRC, LogComponentLevels::gpio()) << "Reading pins ["<<fromPin<<","<<toPin<<"] of register: " << std::hex << gpioRegister;

	if(gpioRegister==::Sca::Constants::GpioFlags::InterruptEnable || gpioRegister==::Sca::Constants::GpioFlags::ClockSelect)
		scasw_throw_runtime_error_with_origin("Cannot use ranged command on flag register");

	if(fromPin<0 || fromPin>31 || toPin<0 || toPin>31) scasw_throw_runtime_error_with_origin("Invalid Pin number");
	if(fromPin>toPin) scasw_throw_runtime_error_with_origin("fromPin > toPin not allowed");

	uint32_t my_register = getRegister(gpioRegister);
	std::vector<bool> my_values;

	for(int pin=fromPin;pin<=toPin;pin++)
	{
		my_values.push_back(  my_register & (1<<pin)  );
	}
	return my_values;	
}
void Gpio::setRegisterRangeToValue(uint32_t gpioRegister, int fromPin, int toPin, bool setValue)
{
	LOG(Log::TRC, LogComponentLevels::gpio()) << "Setting pins ["<<fromPin<<","<<toPin<<"] of register: " << std::hex << gpioRegister<<" to: "<<(setValue ? 1 : 0);

	if(gpioRegister==::Sca::Constants::GpioFlags::InterruptEnable || gpioRegister==::Sca::Constants::GpioFlags::ClockSelect)
		scasw_throw_runtime_error_with_origin("Cannot use ranged command on flag register");	

	if(fromPin<0 || fromPin>31 || toPin<0 || toPin>31) scasw_throw_runtime_error_with_origin("Invalid Pin number");
	if(fromPin>toPin) scasw_throw_runtime_error_with_origin("fromPin > toPin not allowed");

	uint32_t my_register = getRegister(gpioRegister);
	for(int pin=fromPin;pin<=toPin;pin++)//the commands inside the IF are copied from function setRegisterBitValue
	{
		my_register &= ~(1<<pin); //clears the pin-th pin
		if(setValue)
			my_register |= 1<<pin; //if we were to set it to 1, now it will be set to 1
	}
	setRegister(gpioRegister,my_register);
}

Request Gpio::makeSetPinsValuesRequest(  uint32_t registerData, std::map<unsigned int, bool> pinValue  )
{

	LOG(Log::TRC, LogComponentLevels::gpio()) << "Preparing a pin-write request";

	std::bitset<32> dataBits( registerData );

	for ( auto const &i : pinValue ) 
	{
		LOG(Log::TRC, LogComponentLevels::gpio()) << "Set pin " << i.first 
			<< " to value " << std::boolalpha << i.second;
		dataBits[i.first] = i.second;
	}

	registerData = dataBits.to_ulong();

	Request request(
				::Sca::Constants::ChannelIds::GPIO,
				::Sca::Constants::Commands::GPIO_W_DATAOUT,
				{ (uint8_t) (registerData >> 16),
				  (uint8_t) (registerData >> 24),
				  (uint8_t) (registerData >>  0),
				  (uint8_t) (registerData >>  8)
				}
			);

	return request;
}

Request Gpio::makeGetPinsValuesRequest()
{

	LOG(Log::TRC, LogComponentLevels::gpio()) << "Preparing a pin-read request";

	Request request(
				::Sca::Constants::ChannelIds::GPIO,
				::Sca::Constants::Commands::GPIO_R_DATAIN,
				{}
			);

	return request;
}

Request Gpio::makeSetPinsDirectionsRequest( uint32_t registerData, std::map<unsigned int, Gpio::Direction> pinDirection )
{

	LOG(Log::TRC, LogComponentLevels::gpio()) << "Preparing a pin-direction request";

	std::bitset<32> dataBits( registerData );

	for ( auto const &i : pinDirection ) 
	{
		LOG(Log::TRC, LogComponentLevels::gpio()) << "Set pin " << i.first 
			<< " to " << ( (i.second == Gpio::Direction::OUTPUT) ? "output" : "input" ) << " direction";
		dataBits[i.first] = (bool) i.second;
	}

	registerData = dataBits.to_ulong();

	Request request(
				::Sca::Constants::ChannelIds::GPIO,
				::Sca::Constants::Commands::GPIO_W_DIRECTION,
				{ (uint8_t) (registerData >> 16),
				  (uint8_t) (registerData >> 24),
				  (uint8_t) (registerData >>  0),
				  (uint8_t) (registerData >>  8)
				}
			);

	return request;
}

}
