#include <LogIt.h>

#include <Hdlc/BackendFactory.h>

#include <Sca/Defs.h>
#include <Sca/Sca.h>

#include <boost/lexical_cast.hpp>

#include <ScaCommon/except.h>
#include <ScaCommon/ScaSwLogComponents.h>

#include <stdexcept>
#include <vector>
#include <fstream>
#include <iterator>


using boost::lexical_cast;
namespace Sca
{


/**
* DAC Commands
*/

::Sca::Constants::Commands Dac::getReadCommandFromRegister(::Sca::Constants::Dac dacChannel)
{
	switch(dacChannel)
	{
	case ::Sca::Constants::Dac::DAC_A: return ::Sca::Constants::Commands::DAC_R_A; break;
	case ::Sca::Constants::Dac::DAC_B: return ::Sca::Constants::Commands::DAC_R_B; break;
	case ::Sca::Constants::Dac::DAC_C: return ::Sca::Constants::Commands::DAC_R_C; break;
	case ::Sca::Constants::Dac::DAC_D: return ::Sca::Constants::Commands::DAC_R_D; break;
	default: throw std::runtime_error("[GPIO] invalid register input"); break;
	}
}
::Sca::Constants::Commands Dac::getWriteCommandFromRegister(::Sca::Constants::Dac dacChannel)
{
	switch(dacChannel)
	{
	case ::Sca::Constants::Dac::DAC_A: return ::Sca::Constants::Commands::DAC_W_A; break;
	case ::Sca::Constants::Dac::DAC_B: return ::Sca::Constants::Commands::DAC_W_B; break;
	case ::Sca::Constants::Dac::DAC_C: return ::Sca::Constants::Commands::DAC_W_C; break;
	case ::Sca::Constants::Dac::DAC_D: return ::Sca::Constants::Commands::DAC_W_D; break;
	default: throw std::runtime_error("[GPIO] invalid register input"); break;
	}
}
void Dac::writeRegister(::Sca::Constants::Dac dacChannel, uint8_t writeValue)
{
	LOG(Log::TRC, LogComponentLevels::dac()) << "Writing channel: " << std::hex << dacChannel<<" with value: "<<std::hex <<writeValue;
	
	Request request(
			::Sca::Constants::ChannelIds::DAC,
			getWriteCommandFromRegister(dacChannel),
			{0,writeValue,0,0}
			 );
	m_synchronousService.sendAndWaitReply( request );
}

uint8_t Dac::readRegister(::Sca::Constants::Dac dacChannel)
{
	LOG(Log::TRC, LogComponentLevels::dac()) << "Reading channel: " << std::hex << dacChannel;

	Request request(
			::Sca::Constants::ChannelIds::DAC,
			getReadCommandFromRegister(dacChannel),
			{}
			 );

	Reply reply = m_synchronousService.sendAndWaitReply( request );
	return (uint8_t) reply[5];
}
float Dac::getVoltageEstimateAtRegister(::Sca::Constants::Dac dacChannel)
{
	LOG(Log::TRC, LogComponentLevels::dac()) << "Getting voltage estimate of channel: " << std::hex << dacChannel;

	return 1.15*static_cast<int>(readRegister(dacChannel)) / static_cast<int>(0xFF);
}
float Dac::getVoltageStepEstimate()
{
	return 1.15*static_cast<int>(0x01) / static_cast<int>(0xFF);
}
void Dac::incrementChannelByStep(::Sca::Constants::Dac dacChannel)
{
	LOG(Log::TRC, LogComponentLevels::dac()) << "Increasing voltage of channel: " << std::hex << dacChannel<<" by smallest step";

	uint8_t val = readRegister(dacChannel);
	if(val!=0xFF)
	{
		val++;
		writeRegister(dacChannel,val);
	}
}
void Dac::decrementChannelByStep(::Sca::Constants::Dac dacChannel)
{
	LOG(Log::TRC, LogComponentLevels::dac()) << "Reducing voltage of channel: " << std::hex << dacChannel<<" by smallest step";

	uint8_t val = readRegister(dacChannel);
	if(val!=0x00)
	{
		val--;
		writeRegister(dacChannel,val);
	}
}
void Dac::setVoltageOnChannel(::Sca::Constants::Dac dacChannel, float voltageSetValue)
{
	LOG(Log::TRC, LogComponentLevels::dac()) << "Setting V= "<<voltageSetValue<<" on channel: " << std::hex << dacChannel;

	if (voltageSetValue<0.0 || voltageSetValue>1.15)
	    THROW_WITH_ORIGIN(std::range_error, "Required DAC value is out of bounds");
	writeRegister(dacChannel, voltageSetValue/1.15 * 255.0);
	
		
}

}
