/*
 * SpiImpl.cpp
 *
 *  Created on: May 11, 2016
 *      Author: Paris Moschovakos
 */

#include <LogIt.h>
#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/Sca.h>
#include <Sca/ErrorTranslator.h>
#include <boost/lexical_cast.hpp>
#include <ScaCommon/except.h>
#include <ScaCommon/ScaSwLogComponents.h>

#include <stdexcept>
#include <vector>

using boost::lexical_cast;

namespace Sca
{

void Spi::configureBus( unsigned int transmissionBaudRate,
		uint8_t transmissionBitLength,
		bool sampleAtFallingTxEdge,
		bool sampleAtFallingRxEdge,
		bool lsbToMsb,
		bool sclkIdleHigh,
		bool autoSsMode)
{
	LOG(Log::TRC, LogComponentLevels::spi()) << "Configuring SPI bus";

	setTransmissionBaudRate ( transmissionBaudRate );
	setTransmisionBitLength ( transmissionBitLength );

	setTxEdge( sampleAtFallingTxEdge );
	setRxEdge( sampleAtFallingRxEdge );
	setLsbToMsb( lsbToMsb );
	setInvSclk( sclkIdleHigh );

	setSsMode( autoSsMode );

}

void Spi::go()
{
  LOG(Log::TRC, LogComponentLevels::spi() ) << "Executing SPI transmission";

	Request requestGo(
	    	::Sca::Constants::ChannelIds::SPI,
	    	::Sca::Constants::Commands::SPI_GO,
	    	{} );
	Reply reply = m_synchronousService.sendAndWaitReply( requestGo );
	throwIfScaReplyError(reply);
}

void Spi::writeSlave ( int selectedSlave, std::vector<uint8_t> &payload, bool toggleSS )
{

	int spiIter = getSpiIterations( payload );
	LOG(Log::DBG, LogComponentLevels::spi()) << "Writing to SPI slave: " << selectedSlave ;

	if (toggleSS)
		setSelectedSlave( selectedSlave );

	std::vector<uint8_t>::iterator dataIter;

	for ( int i = 0; i < spiIter; i++ )
	{

		dataIter = payload.begin() + (m_transmissionBitLength / 8) * i;
		LOG(Log::TRC, LogComponentLevels::spi()) << "Chunk " << i + 1 << " out of " << spiIter ;
		writeSpiSlaveChunk ( dataIter );

	}

	if (toggleSS)
		resetSelectedSlaves();

}

// Paris: Used in cases in which SS needs to be enabled for more than one slaves
void Spi::writeSlaves ( uint8_t selectedSlaves, std::vector <uint8_t> &payload, bool toggleSS )
{

	int spiIter = getSpiIterations( payload );
	LOG(Log::INF, LogComponentLevels::spi()) << "Writing to SPI slaves with mask: 0x" << std::hex << (int)selectedSlaves << std::dec;

	if (toggleSS)
		setSelectedSlaves( selectedSlaves );

	std::vector<uint8_t>::iterator dataIter;

	for ( int i = 0; i < spiIter; i++ )
	{

		dataIter = payload.begin() + (m_transmissionBitLength / 8) * i;
		LOG(Log::TRC) << "Chunk " << i + 1 << " out of " << spiIter ;
		writeSpiSlaveChunk ( dataIter );

	}

	if (toggleSS)
		resetSelectedSlaves();

}

void Spi::setSelectedSlave ( int slaveId )
{
	if (slaveId >= 8)
	    throw std::out_of_range("setSelectedSlave: argument out of range: "+slaveId );

	LOG(Log::TRC, LogComponentLevels::spi()) << "Set SS for slave: " << slaveId;

	uint8_t slaveIdMask = 0x00;
	slaveIdMask |= 1 << slaveId;

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_SS,
	    {0, 0, slaveIdMask, 0} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);
}

void Spi::setSelectedSlaves ( uint8_t slaveIdMask )
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_SS,
	    {0, 0, slaveIdMask, 0} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

	LOG(Log::TRC, LogComponentLevels::spi()) << "Set SS mask: " << std::hex << "0x" << slaveIdMask << std::dec;
}

void Spi::resetSelectedSlaves ()
{

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_SS,
	    {0, 0, 0, 0} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

	LOG(Log::TRC, LogComponentLevels::spi()) << "Reset all SS";
}

uint8_t Spi::getSelectedSlaves ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_SS,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

	LOG(Log::TRC, LogComponentLevels::spi()) << "Get SS mask: 0x" << std::hex << reply[6] << std::dec;

	return reply[6];
}

std::vector<uint8_t> Spi::getSlaveReplies ( int numberOfChunks, uint8_t selectedSlave, bool toggleSS )
{

	LOG(Log::INF, LogComponentLevels::spi()) << "Reading " << numberOfChunks << " number of chunks from SPI slave: "
			<< (int)selectedSlave;

	if (toggleSS)
		setSelectedSlave( selectedSlave );

	std::vector<uint8_t> replies, tmp;

	for ( int i = 0; i < numberOfChunks; i++ )
	{
		tmp = getSlaveReply( selectedSlave, toggleSS );
		for(const auto& j : tmp)
			replies.push_back(j);
	}

	if (toggleSS)
	    	resetSelectedSlaves();

	return replies;
}

Reply Spi::getDataRegister( uint8_t miso )
{
	LOG(Log::TRC, LogComponentLevels::spi()) << "Get MISO data register: " << (int) miso;

	Request request(
		::Sca::Constants::ChannelIds::SPI,
		miso,
	{} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

	return reply;
}

Reply Spi::setDataRegister( uint8_t mosi, std::vector<uint8_t>::iterator &dataIter )
{
	LOG(Log::TRC, LogComponentLevels::spi()) << "Set MOSI data register: " << (int) mosi;

	Request request(
		::Sca::Constants::ChannelIds::SPI,
		mosi,
		{ *++dataIter, *--dataIter, *(dataIter+=3), *--dataIter } );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

	return reply;
}

std::vector<uint8_t> Spi::getSlaveReply ( uint8_t selectedSlave, bool toggleSS )
{

	LOG(Log::TRC, LogComponentLevels::spi()) << "SPI read chunk from " << (int)selectedSlave <<
			" with toggling SS set to: " << toggleSS;

	go();

	std::vector<uint8_t> miso;

	for ( int i =  ( m_transmissionBitLength-1 ) / 32; i >= 0; i--)
	{
	  Reply reply = getDataRegister(::Sca::Constants::Commands::SPI_R_MISO0 + i * (::Sca::Constants::Spi::SPI_MISO_REGISTERS_DISTANCE) );
	  miso.push_back(reply[5]); 
	  miso.push_back(reply[4]);
	  miso.push_back(reply[7]); 
	  miso.push_back(reply[6]);
	}

	LOG(Log::TRC, LogComponentLevels::spi()) << "SPI replies:";
	for (auto const& i: miso)
	  LOG(Log::TRC, LogComponentLevels::spi()) << std::hex << "0x" << (int)i;

	return miso;

}

float Spi::getTransmissionBaudRate ()
{

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_FREQ,
	    {} );
	request.setLength(1);
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);
	int divider = reply[7] << 8 | reply[6] ;

	m_baudRate = 2. / (divider + 1) * 10000000;

	LOG(Log::TRC, LogComponentLevels::spi()) << "Get transmission Baud rate: " << m_baudRate << "Hz";

	return m_baudRate;
}

void Spi::setTransmissionBaudRate ( unsigned int transmissionBaudRate )
{

	if ( transmissionBaudRate < 305 || transmissionBaudRate > 20000000 )
	    throw std::runtime_error("setTransmissionBaudRate: argument out of range: "+transmissionBaudRate );

	LOG(Log::TRC, LogComponentLevels::spi()) << "Set transmission Baud rate to: " << transmissionBaudRate << "Hz";

	auto divider = (2 * 10000000 / transmissionBaudRate) - 1;
	if ( divider > 65535 ) divider = 65535;
	m_baudRate = 2. / ((divider + 1) * 10000000);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_FREQ,
	    {0, 0, (uint8_t) divider, (uint8_t) (divider >> 8)} );

	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

}

uint8_t Spi::getTransmisionBitLength ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);
	uint8_t length = reply[6] & 0x7f;

	// Value 0 represents 128 bits
	if ( !length )
		length = 128;

	LOG(Log::TRC, LogComponentLevels::spi()) << "Get transmission bit length: " << (int)length;

	return length;
}

void Spi::setTransmisionBitLength ( unsigned int transmissionBitLength )
{

	if ( transmissionBitLength > 128 || transmissionBitLength < 1 )
	    throw std::out_of_range( "setTransmisionBitLength: argument out of range: " + transmissionBitLength );

	LOG(Log::TRC, LogComponentLevels::spi()) << "Set transmission bit length: " << transmissionBitLength;

	m_transmissionBitLength = transmissionBitLength;
	if ( transmissionBitLength == 128 )
		transmissionBitLength = 0;

	uint16_t currentReg = getControlRegister();
	uint16_t updatedCtrlReg = ((uint8_t)transmissionBitLength << 8) | ( currentReg & 0x80FF );

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

}

bool Spi::getInvSclk ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);
	bool isSclkInverted = reply[6] & 0x80;

	LOG(Log::TRC, LogComponentLevels::spi()) << "Get SCLK idle level high: " << isSclkInverted;

	return isSclkInverted;
}

void Spi::setInvSclk ( bool setSclkInv )
{

	LOG(Log::TRC, LogComponentLevels::spi()) << "Set SCLK idle level high: " << setSclkInv;

	uint16_t currentReg = getControlRegister ();
	m_setSclkInv = setSclkInv;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setSclkInv ^ updatedCtrlReg) & (1 << 15);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

}

bool Spi::getRxEdge ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);
	bool sampleAtFallingEdge = reply[7] & 0x02;
	// '0': sample at the rising edge / '1': sample at the falling edge

	LOG(Log::TRC, LogComponentLevels::spi()) << "Get sample Rx at the falling edge: " << sampleAtFallingEdge;

	return sampleAtFallingEdge;
}

void Spi::setRxEdge ( bool setRxEdge )
{

	LOG(Log::TRC, LogComponentLevels::spi()) << "Set Rx to sample at the falling edge: " << setRxEdge;

	uint16_t currentReg = getControlRegister ();
	m_setRxEdge = setRxEdge;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setRxEdge ^ updatedCtrlReg) & (1 << 1);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

}

bool Spi::getTxEdge ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);
	bool sampleAtFallingEdge = reply[7] & 0x04;
	// '0': sample at the rising edge / '1': falling at the rising edge

	LOG(Log::TRC, LogComponentLevels::spi()) << "Get sample Tx at the falling edge: " << sampleAtFallingEdge;

	return sampleAtFallingEdge;
}

void Spi::setTxEdge ( bool setTxEdge )
{

	LOG(Log::TRC, LogComponentLevels::spi()) << "Set Tx to sample at the falling edge: " << setTxEdge;

	uint16_t currentReg = getControlRegister ();
	m_setTxEdge = setTxEdge;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setTxEdge ^ updatedCtrlReg) & (1 << 2);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

}

bool Spi::getLsbToMsb ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);
	bool lsbToMsb = reply[7] & 0x08;
	// '0': send from MSB to LSB / '1': send from LSB to MSB

	LOG(Log::TRC, LogComponentLevels::spi()) << "Got LSB to MSB: " << lsbToMsb;

	return lsbToMsb;
}

void Spi::setLsbToMsb ( bool setMsbToLsb )
{

	LOG(Log::TRC, LogComponentLevels::spi()) << "Setting MsbToLsb: " << setMsbToLsb;

	uint16_t currentReg = getControlRegister ();
	m_setMsbToLsb = setMsbToLsb;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setMsbToLsb ^ updatedCtrlReg) & (1 << 3);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

}

bool Spi::getSsMode ()
{

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);
	bool ssMode = reply[7] & 0x20;
	// '0': manual mode / '1': automatic mode

	LOG(Log::TRC, LogComponentLevels::spi()) << "Got automatic slave select mode: " << ssMode;

	return ssMode;
}

void Spi::setSsMode ( bool setSsMode )
{
	LOG(Log::TRC, LogComponentLevels::spi()) << "Setting automatic slave select mode to: " << setSsMode;

	uint16_t currentReg = getControlRegister ();
	m_setSsMode = setSsMode;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setSsMode ^ updatedCtrlReg) & (1 << 5);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

}

void Spi::setSpiMode ( unsigned int spiMode )
{
/*
 *
   +----------+----------------+-------------+------------+
   | SPI Mode | Clock Polarity | Clock Phase | Clock Edge |
   |          |      CPOL      |     CPHA    | CKE/NCPHA  |
   +----------+----------------+-------------+------------+
   |    0     |       0        |      0      |     1      |
   +----------+----------------+-------------+------------+
   |    1     |       0        |      1      |     0      |
   +----------+----------------+-------------+------------+
   |    2     |       1        |      0      |     1      |
   +----------+----------------+-------------+------------+
   |    3     |       1        |      1      |     0      |
   +----------+----------------+-------------+------------+
 *
 */

	LOG(Log::TRC, LogComponentLevels::spi()) << "Setting SPI mode: " << spiMode;

	if ( spiMode > 3 || spiMode < 0 )
	    throw std::out_of_range( "setSpiMode: argument out of range: " + spiMode );

	switch (spiMode) {
		case 0 :
			setInvSclk(false);
			setRxEdge(false);
			setTxEdge(false);
			break;
		case 1 :
			setInvSclk(false);
			setRxEdge(true);
			setTxEdge(true);
			break;
		case 2 :
			setInvSclk(true);
			setRxEdge(true);
			setTxEdge(true);
			break;
		case 3 :
			setInvSclk(true);
			setRxEdge(false);
			setTxEdge(false);
			break;
	}

}

unsigned int Spi::getSpiMode ()
{
	uint16_t ctrlReg = getControlRegister();
	bool invClk = (ctrlReg >> 15) & 1;
	bool rxEdge = (ctrlReg >> 1) & 1;
	bool txEdge = (ctrlReg >> 2) & 1;

	// returns -1 if no common SPI mode is found
	int spiMode = -1;

	if (invClk && rxEdge && txEdge)
		spiMode = 2;
	else if ( invClk && !rxEdge && !txEdge )
		spiMode = 3;
	else if ( !invClk && rxEdge && txEdge )
		spiMode = 1;
	else if ( !invClk && !rxEdge && !txEdge )
		spiMode = 0;

	LOG(Log::TRC, LogComponentLevels::spi()) << "Getting SPI mode: " << spiMode;

	return spiMode;
}

uint16_t Spi::getControlRegister()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request );
	throwIfScaReplyError(reply);

	LOG(Log::TRC, LogComponentLevels::spi()) << "Getting control register";

	return (reply[6] << 8) | reply[7] ;
}

void Spi::writeSpiSlaveChunk ( std::vector<uint8_t>::iterator &dataIter )
{

    if (m_transmissionBitLength > 96)
    {
        setDataRegister(::Sca::Constants::Commands::SPI_W_MOSI3, dataIter);
        *(dataIter+=2);
    }

    if (m_transmissionBitLength > 64)
    {
    	setDataRegister(::Sca::Constants::Commands::SPI_W_MOSI2, dataIter);
    	*(dataIter+=2);
    }

    if (m_transmissionBitLength > 32)
	{
    	setDataRegister(::Sca::Constants::Commands::SPI_W_MOSI1, dataIter);
    	*(dataIter+=2);
	}

    setDataRegister(::Sca::Constants::Commands::SPI_W_MOSI0, dataIter);
    *(dataIter+=2);

    go();

}

unsigned int Spi::getSpiIterations ( std::vector<uint8_t> &payload )
{

	LOG(Log::TRC, LogComponentLevels::spi()) << "Acquiring amount of SPI transactions";

	if ( (payload.size() * 8) % m_transmissionBitLength > 7 || payload.size() == 0 )
		throw std::runtime_error("Payload size is not valid: "+lexical_cast<std::string>(payload.size())+" byte(s)" );

	return (payload.size() * 8) / m_transmissionBitLength;
}

} /* namespace Sca */

